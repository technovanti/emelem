module.exports = {
  presets: [
    ['@vue/app', {
      modules: 'commonjs'
    }],
    '@babel/preset-env'
  ]
}
