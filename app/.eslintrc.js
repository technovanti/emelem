module.exports = {
  root: true,
  env: {
    browser: true
  },
  extends: [
    'plugin:vue/strongly-recommended',
    '@vue/standard'
  ],
  rules: {
    'arrow-parens': 0,
    'generator-star-spacing': 0,
    'no-multiple-empty-lines': ['error', { max: 1, maxEOF: 1, maxBOF: 1 }],
    'brace-style': ['error', '1tbs', { allowSingleLine: true }],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/require-prop-types': 0,
    'vue/require-default-prop': 0,
    'vue/valid-v-for': 0,
    'vue/max-attributes-per-line': [2, { singleline: 3 }]
  },
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  }
}
