module.exports = {
  publicPath: process.env.VUE_APP_PUBLIC_PATH,
  assetsDir: 'assets',
  devServer: {
    https: false,
    hot: true,
    proxy: {
      '^/wp-json': {
        target: process.env.VUE_APP_API,
        ws: true,
        changeOrigin: true
      }
    }
  },
  productionSourceMap: process.env.NODE_ENV !== 'production',
  lintOnSave: process.env.NODE_ENV !== 'production',
  transpileDependencies: ['vuetify'],
  css: {
    extract: process.env.NODE_ENV !== 'development',
    sourceMap: process.env.NODE_ENV !== 'production',
    loaderOptions: {
      sass: {
        data: '@import \'~@/assets/styles/main.scss\''
      }
    }
  },
  chainWebpack: (config) => {
    ['vue-modules', 'vue', 'normal-modules', 'normal'].forEach((match) => {
      config.module.rule('scss').oneOf(match).use('sass-loader')
        .tap(opt => Object.assign(opt, { data: '@import \'~@/assets/styles/main.scss\';' }))
    })
  }
}
