import Vue from 'vue'
import Router from 'vue-router'
import pages from './modules/routes'
import store from './store'
import Cookies from 'js-cookie'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: pages
})

router.beforeEach((to, from, next) => {
  const cookies = Cookies.getJSON('EMELEM_SECURE_AUTH_COOKIE')

  if (to.query.referral_code) {
    store.dispatch('dashboard/setSponsorId', to.query.referral_code)
  }

  if (!store.state.auth.loggedIn && cookies) {
    store.dispatch('auth/validate', cookies)
  } else if (!store.state.auth.loggedIn && to.path !== '/') {
    next('/')
  }

  let title = 'Technovanti - '
  if (to.name === 'dashboard') {
    title += !store.state.auth.loggedIn ? to.meta.title.login : to.meta.title.default

    store.dispatch('loading', true)
  } else {
    title += to.meta.title
  }

  document.title = title

  next()
})

router.afterEach((to, from) => {
  store.dispatch('loading', false)
})

export default router
