import state from './state'
import actions from './actions'
import mutations from './mutations'

export const accounts = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters: {}
}
