export default {
  setInactiveUsers (state, inactiveUsers) {
    state.inactiveUsers = inactiveUsers
  },

  setActiveUsers (state, activeUsers) {
    state.activeUsers = activeUsers
  },

  setSubUsers (state, subUsers) {
    state.users = subUsers
  },

  videoId (state, videoId) {
    state.videoId = videoId
  },

  watched (state, data) {
    state.watched = data.watched
  }
}
