import api from '@/lib/api'

export default {
  addUser (user) {
    return api.post('emelem/v2/users/add', user)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  searchUsers () {
    return api.get('emelem/v2/users/search')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getInactiveUsers () {
    return api.get('emelem/v2/users/getInactiveUsers')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getActiveUsers () {
    return api.get('emelem/v2/users/getActiveUsers')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getSubUsers () {
    return api.get('emelem/v2/users/getSubUsers')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getDailyVideo () {
    return api.get('emelem/v2/users/getDailyVideo')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getWatchedToday () {
    return api.get('emelem/v2/users/watched')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  edit (user) {
    return api.post(`emelem/v2/users/update_password`, user)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  }
}
