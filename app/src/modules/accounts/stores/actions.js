import service from './service'

export default {
  addUser ({ commit }, user) {
    return service.addUser(user)
      .then(response => response)
      .catch(errors => ({ errors: errors }))
  },

  searchUsers ({ commit }) {
    return service.searchUsers()
  },

  getInactiveUsers ({ commit }) {
    return service.getInactiveUsers()
      .then(inactiveUsers => commit('setInactiveUsers', inactiveUsers))
  },

  getActiveUsers ({ commit }) {
    return service.getActiveUsers()
      .then(activeUsers => commit('setActiveUsers', activeUsers))
  },

  getSubUsers ({ commit }) {
    return service.getSubUsers()
      .then(subUsers => commit('setSubUsers', subUsers))
  },

  getDailyVideo ({ commit }) {
    return service.getDailyVideo()
      .then(videoId => commit('videoId', videoId))
      .catch(errors => ({ errors: errors }))
  },

  getWatchedToday ({ commit }) {
    return service.getWatchedToday()
      .then(watched => commit('watched', watched))
      .catch(errors => ({ errors: errors }))
  },

  edit ({ commit }, user) {
    return service.edit(user)
      .then(response => response)
      .catch(errors => ({ errors: errors }))
  }
}
