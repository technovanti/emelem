export default {
  path: '/accounts',
  name: 'accounts',
  component: () => import('./Accounts.vue'),
  meta: {
    title: 'Accounts',
    menu: {
      text: 'Accounts',
      icon: 'supervisor_account'
    }
  }
}
