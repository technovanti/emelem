import service from './service'

export default {

  downlines ({ commit }) {
    return service.downlines()
      .then(downlines => commit('downlines', downlines))
      .catch(error => error)
  },

  getDownlines ({ commit }, user) {
    return service.getDownlines(user)
      .then(downlines => commit('downlines', downlines))
      .catch(error => error)
  },

  getLegs ({ commit }) {
    return service.getLegs()
      .then(legs => commit('legs', legs))
      .catch(error => error)
  }
}
