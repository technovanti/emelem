import api from '@/lib/api'

export default {
  downlines () {
    return api.get('emelem/v2/trees')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getDownlines (user) {
    return api.get(`emelem/v2/trees/${user}`)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getLegs () {
    return api.get('emelem/v2/trees/legs')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  }
}
