export default {
  downlines (state, downlines) {
    state.downlines = downlines
  },

  legs (state, legs) {
    state.legs = legs
  }
}
