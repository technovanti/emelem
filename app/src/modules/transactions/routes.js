export default {
  path: '/transactions',
  name: 'transactions',
  component: () => import('./Transactions.vue'),
  meta: {
    title: 'Transactions',
    menu: {
      text: 'Transactions',
      icon: 'transform'
    }
  }
}
