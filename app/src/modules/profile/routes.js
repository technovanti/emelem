export default {
  path: '/profile',
  name: 'profile',
  component: () => import('./Edit.vue'),
  meta: {
    title: 'Profile',
    menu: {
      text: 'Profile',
      icon: 'mdi-account-edit'
    }
  }
}
