import state from './state'
import actions from './actions'
import mutations from './mutations'

export const activationcodes = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters: {}
}
