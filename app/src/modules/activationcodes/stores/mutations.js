export default {
  codes (state, codes) {
    state.codes = codes
  },

  setAvailableCodes (state, availableCodes) {
    state.availableCodes = availableCodes
  },

  setCodesHistory (state, codesHistory) {
    state.codesHistory = codesHistory
  }
}
