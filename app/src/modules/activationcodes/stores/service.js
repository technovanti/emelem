import api from '@/lib/api'

export default {
  getCodes () {
    return api.get('emelem/v2/activationcodes')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getCodesHistory () {
    return api.get('emelem/v2/activationcodes/getCodesHistory')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  transferCode (code) {
    return api.post('emelem/v2/activationcodes/transferCode', code)
  },

  getAvailableCodes () {
    return api.get('emelem/v2/activationcodes/getAvailableCodes')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  activateUser (user) {
    return api.post('emelem/v2/activationcodes/activateUser', user)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message) || error)
  }
}
