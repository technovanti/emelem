import service from './service'

export default {

  getCodes ({ commit }) {
    return service.getCodes()
      .then(codes => commit('codes', codes))
      .catch(error => error)
  },

  getCodesHistory ({ commit }) {
    return service.getCodesHistory()
      .then(codesHistory => commit('setCodesHistory', codesHistory))
      .catch(error => error)
  },

  transferCode ({ commit, dispatch }, code) {
    return service.transferCode(code)
      .then(() => dispatch('getCodes'))
      .catch(error => error)
  },

  getAvailableCodes ({ commit }) {
    return service.getAvailableCodes()
      .then(availableCodes => commit('setAvailableCodes', availableCodes))
      .catch(error => error)
  },

  activateUser ({ commit, dispatch }, user) {
    return service.activateUser(user)
      .then(activated => {
        dispatch('accounts/getInactiveUsers', {}, { root: true })
        dispatch('activationcodes/getAvailableCodes', {}, { root: true })
        dispatch('trees/downlines', {}, { root: true })
      })
      .catch(error => error)
  }
}
