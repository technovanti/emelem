import { auth } from './auth/stores'
import { admin } from './admin/stores'
import { dashboard } from './dashboard/stores'
import { accounts } from './accounts/stores'
import { watch } from './watch/stores'
import { activationcodes } from './activationcodes/stores'
import { trees } from './trees/stores'
import { rewards } from './rewards/stores'
import { encashments } from './encashments/stores'

export default {
  auth,
  admin,
  dashboard,
  accounts,
  watch,
  activationcodes,
  trees,
  rewards,
  encashments
}
