import service from './service'

export default {
  getEncashments ({ commit }) {
    return service.getEncashments()
      .then(encashments => commit('setEncashments', encashments))
  },

  addEncashment ({ commit }, encashment) {
    return service.addEncashments(encashment)
  }
}
