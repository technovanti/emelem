export default {
  setEncashments (state, encashments) {
    state.encashments = encashments
    state.total = 0
    state.unpaid = 0
    encashments.forEach(encashment => {
      state.total += encashment.status !== 3 || encashment.deleted_at === '0000-00-00 00:00:00' ? parseFloat(encashment.amount) : 0
      state.unpaid += encashment.status < 2 || encashment.deleted_at !== '0000-00-00 00:00:00' ? parseFloat(encashment.amount) : 0
    }, state)
  }
}
