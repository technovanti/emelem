import api from '@/lib/api'

export default {
  getEncashments () {
    return api.get('emelem/v2/encashments')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  addEncashments (encashment) {
    return api.post('emelem/v2/encashments', encashment)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  }
}
