export default {
  path: '/encashments',
  name: 'encashments',
  component: () => import('./Encashments.vue'),
  meta: {
    title: 'Encashments',
    menu: {
      text: 'Encashments',
      icon: 'local_atm'
    }
  }
}
