import api from '@/lib/api'
import Cookies from 'js-cookie'

export default {

  login (user) {
    return api.post('emelem/v2/auth/login', user)
      .then(response => {
        Cookies.set('EMELEM_SECURE_AUTH_COOKIE', response.data)
        return response.data
      })
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  logout () {
    Cookies.remove('EMELEM_SECURE_AUTH_COOKIE')
  },

  validate (cookies) {
    return api.post('emelem/v2/auth/validate', {}, { headers: { 'Authorization': `Bearer ${cookies.token}` } })
      .then(response => cookies)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getUser () {
    return api.get(`emelem/v2/auth/user`)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  resetPassword (credentials) {
    return api.post('emelem/v2/auth/resetPassword', credentials)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  }
}
