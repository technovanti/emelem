import initialState from './state'

export default {
  session (state, session) {
    state.token = session.token
    state.role = session.role
    state.loggedIn = true
  },

  user (state, user) {
    state.user = user
  },

  logout (state) {
    state.loggedIn = false
  },

  reset (state) {
    const s = initialState()
    Object.keys(s).forEach(key => {
      state[key] = s[key]
    })
  }
}
