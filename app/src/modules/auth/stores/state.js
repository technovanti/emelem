export default function initialState () {
  return {
    loggedIn: false,
    token: undefined,
    role: undefined,
    user: {
      id: undefined,
      parent: undefined,
      referrer: undefined,
      login: undefined,
      email: undefined,
      firstname: undefined,
      lastname: undefined,
      phone: undefined,
      registered_at: undefined,
      activated_at: undefined,
      code: undefined
    }
  }
}
