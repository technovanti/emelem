import service from './service'

export default {

  login ({ commit, dispatch }, credentials) {
    return service.login(credentials)
      .then(session => {
        commit('session', { token: session.token, role: session.role })
        dispatch('getUser')
      })
      .catch(error => error)
  },

  getUser ({ commit }) {
    return service.getUser()
      .then(user => commit('user', user))
      .catch(error => console.log(error))
  },

  validate ({ commit, dispatch }, cookies) {
    return service.validate(cookies)
      .then(session => {
        commit('session', { token: session.token, role: session.role })
        dispatch('getUser')
      })
      .catch(error => console.log(error))
  },

  resetPassword ({ commit }, credentials) {
    return service.resetPassword(credentials)
      .then(response => response)
      .catch(errors => ({ errors: errors }))
  },

  logout ({ commit }) {
    commit('logout')
    commit('reset')
    return service.logout()
  }
}
