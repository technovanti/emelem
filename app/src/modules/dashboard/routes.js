export default {
  path: '/',
  name: 'dashboard',
  components: {
    default: () => import('./Home.vue'),
    login: () => import('./Login.vue')
  },
  meta: {
    title: {
      default: 'Dashboard',
      login: 'Login'
    },
    menu: {
      text: 'Dashboard',
      icon: 'dashboard'
    }
  }
}
