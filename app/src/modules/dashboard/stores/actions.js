import service from './service'

export default {
  getSponsorId ({ commit }) {
    return service.getSponsorId()
      .then(sponsorId => {
        commit('setSponsorId', sponsorId)
        return sponsorId
      })
  },

  setSponsorId ({ commit, dispatch }, code) {
    return service.setSponsorId(code)
      .then(() => dispatch('getSponsorId'))
  }
}
