import api from '@/lib/api'

export default {
  getSponsorId () {
    return api.get('emelem/v2/users/getReferrer')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  setSponsorId (code) {
    return api.post('emelem/v2/users/setReferrer', { referral_code: code })
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  }
}
