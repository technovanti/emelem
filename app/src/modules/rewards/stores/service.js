import api from '@/lib/api'

export default {
  getRewards () {
    return api.get('emelem/v2/rewards')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getAvailableRewards (code) {
    return api.get('emelem/v2/rewards/getAvailableRewards')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getWatchVideo () {
    return api.get('emelem/v2/rewards/watchvideo')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  addWatchVideo (youtube) {
    return api.post('emelem/v2/rewards/watchvideo', {
      youtube: youtube
    })
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  }
}
