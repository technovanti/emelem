export default {
  setRewards (state, rewards) {
    state.rewards = rewards.rewards
    state.available = rewards.available
    state.flushouts = rewards.flushouts
    state.encashment = rewards.encashment
  },

  setAvailableRewards (state, available) {
    state.available = available
  },

  setYoutube (state, youtube) {
    state.rewards.youtube.total = youtube
  }
}
