import service from './service'

export default {
  getRewards ({ commit }) {
    return service.getRewards()
      .then(rewards => commit('setRewards', rewards))
  },

  getAvailableRewards ({ commit }) {
    return service.getAvailableRewards()
      .then(available => commit('setAvailableRewards', available))
  },

  getWatchVideo ({ commit }) {
    return service.getWatchVideo()
      .then(youtube => commit('setYoutube', youtube))
  },

  addWatchVideo ({ dispatch, commit, rootState }, youtube) {
    const watched = rootState.accounts.watched
    if (!watched) {
      return service
        .addWatchVideo(youtube)
        .then(youtube => {
          commit('setYoutube', youtube)
          if (youtube.getCurrentTime() >= youtube.getDuration()) {
            commit('accounts/watched', { watched: true }, { root: true })
          } else if (youtube.getCurrentTime() >= 60) {
            commit('accounts/watched', { watched: true }, { root: true })
          }
        })
        .catch(error => error)
    }
  }
}
