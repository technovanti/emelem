export default {
  rewards: {
    signup: {
      rewards: [],
      total: 0
    },
    youtube: {
      rewards: [],
      total: 0
    },
    customer_referral: {
      rewards: [],
      total: 0
    },
    leadership_matching: {
      rewards: [],
      total: 0
    },
    topmax: {
      rewards: [],
      total: 0
    }
  },
  available: {
    signup: 0,
    youtube: 0,
    customer_referral: 0,
    leadership_matching: 0,
    topmax: 0,
    total: 0
  },
  flushouts: {
    customer_referral: {
      flushouts: [],
      total: 0
    },
    leadership_matching: {
      flushouts: [],
      total: 0
    },
    topmax: {
      flushouts: [],
      total: 0
    },
    total: 0
  },
  encashment: 0
}
