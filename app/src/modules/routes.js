import admin from './admin/routes'
import dashboard from './dashboard/routes'
import profile from './profile/routes'
import accounts from './accounts/routes'
import watch from './watch/routes'
import rewards from './rewards/routes'
import activationcodes from './activationcodes/routes'
import trees from './trees/routes'
import encashments from './encashments/routes'

export default [
  admin,
  dashboard,
  profile,
  accounts,
  watch,
  rewards,
  activationcodes,
  trees,
  encashments
]
