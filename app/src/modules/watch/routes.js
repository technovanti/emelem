export default {
  path: '/watch',
  name: 'watch',
  component: () => import('./Watch.vue'),
  meta: {
    title: 'Watch and Earn',
    menu: {
      text: 'Watch',
      icon: 'ondemand_video'
    }
  }
}
