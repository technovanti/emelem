import service from './service'

export default {
  getWatch ({ commit }) {
    return service.getWatch()
      .then(watch => commit('setWatch', watch))
  },
  addWatch ({ commit, dispatch }, player) {
    return service.addWatch(player)
      .then(() => dispatch('getWatch'))
  }
}
