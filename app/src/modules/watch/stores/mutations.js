export default {
  setWatch (state, watch) {
    state.watched = watch.watched
    state.total = watch.total
    state.videoId = watch.videoId
  }
}
