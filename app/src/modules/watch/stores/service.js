import api from '@/lib/api'

export default {
  getWatch () {
    return api.get('emelem/v2/watch')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  addWatch (player) {
    return api.post('emelem/v2/watch', { youtube: player })
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  }
}
