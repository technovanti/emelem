import api from '@/lib/api'

export default {
  add (user) {
    return api.post('emelem/v2/users/admin', user)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  users () {
    return api.get('emelem/v2/users/admin')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  addCodes (codes) {
    return api.post('emelem/v2/activationcodes/admin', codes)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  codes () {
    return api.get('emelem/v2/activationcodes/admin')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getEncashments () {
    return api.get('emelem/v2/admin/encashments')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  editEncashments (item) {
    return api.put('emelem/v2/admin/encashments', item)
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  rejectEncashments (item) {
    return api.delete('emelem/v2/admin/encashments', { data: { item } })
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  },

  getEncashmentDates () {
    return api.get('emelem/v2/admin/encashmentDates')
      .then(response => response.data)
      .catch(error => Promise.reject(error.response.data.message || error))
  }
}
