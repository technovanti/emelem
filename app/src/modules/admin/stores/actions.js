import service from './service'

export default {

  add ({ commit }, user) {
    return service.add(user)
      .then(response => response)
      .catch(error => error)
  },

  users ({ commit }) {
    return service.users()
      .then(users => commit('users', users))
      .catch(error => error)
  },

  codes ({ commit }) {
    return service.codes()
      .then(codes => commit('codes', codes))
      .catch(error => error)
  },

  addCodes ({ commit }, codes) {
    return service.addCodes(codes)
      .then(response => response)
      .catch(error => error)
  },

  getEncashments ({ commit }) {
    return service.getEncashments()
      .then(encashments => commit('setEncashments', encashments))
      .catch(error => error)
  },

  editEncashments ({ commit }, item) {
    return service.editEncashments(item)
      .then(encashment => commit('setEncashment', encashment))
      .catch(error => error)
  },

  rejectEncashments ({ commit }, item) {
    return service.rejectEncashments(item)
      .then(encashment => commit('setEncashment', encashment))
      .catch(error => error)
  },

  getEncashmentDates ({ commit }) {
    return service.getEncashmentDates()
      .then(dates => commit('setEncashmentDates', dates))
      .catch(error => error)
  }
}
