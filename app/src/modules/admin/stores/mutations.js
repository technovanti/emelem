export default {
  users (state, users) {
    state.users = users
  },

  codes (state, codes) {
    state.codes = codes
  },

  setEncashments (state, encashments) {
    state.encashments = encashments
  },

  setEncashment (state, encashment) {
    let key = state.encashments.findIndex(item => item.encashment_id === encashment.encashment_id)
    state.encashments[key] = encashment
  },

  setEncashmentDates (state, dates) {
    state.encashmentDates = dates
  }
}
