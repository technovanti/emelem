export default {
  path: '/admin',
  name: 'admin',
  component: () => import('./Admin.vue'),
  meta: {
    title: 'Administration',
    menu: false
  }
}
