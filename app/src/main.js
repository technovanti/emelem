// Added support for ES6
import 'babel-polyfill'

import Vue from 'vue'
import VueTheMask from 'vue-the-mask'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import youtube from './plugins/youtube'
import excel from 'vue-excel-export'

Vue.use(VueTheMask)
Vue.use(Vuelidate)
Vue.use(excel)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  youtube,
  render: h => h(App)
}).$mount('#app')
