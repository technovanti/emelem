import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules/stores'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
    requests: 0
  },
  mutations: {
    loading (state, progress) {
      if (state.requests > 0) {
        state.requests = progress ? state.requests + 1 : state.requests - 1
      } else {
        state.requests = progress ? state.requests + 1 : 0
      }

      if (state.requests === 0) {
        state.loading = false
      } else {
        state.loading = progress
      }
    }
  },
  actions: {
    loading ({ commit }, progress) {
      commit('loading', progress)
    }
  },
  getters: {
    loading: state => state.loading
  },
  modules
})
