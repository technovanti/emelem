export default function pesos (amount) {
  try {
    amount = parseFloat(amount).toFixed(2)
    amount = (isNaN(amount)) ? '₱0.00' : '₱' + amount.replace(/\d(?=(\d{3})+\.)/g, '$&,')
    return amount
  } catch (e) {
    throw new Error(e)
  }
}

export function unpesos (amount) {
  try {
    amount = parseFloat(amount.replace(/(₱|,)/g, '') || 0)
    return isNaN(amount) ? '' : amount
  } catch (e) {
    throw new Error(e)
  }
}
