import axios from 'axios'
import store from '@/store'

const api = axios.create({ baseURL: '/wp-json/' })

api.interceptors.request.use(config => {
  store.dispatch('loading', true)
  const token = store.state.auth.token

  if (token) {
    config.headers.Authorization = `Bearer ${token}`
    config.headers['Content-Type'] = 'application/json'
  }

  return config
}, error => {
  return Promise.reject(error)
})

api.interceptors.response.use(response => {
  store.dispatch('loading', false)
  return response
}, error => {
  store.dispatch('loading', false)
  return Promise.reject(error)
})

export default api
