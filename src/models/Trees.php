<?php
namespace Stragidex\Models;
use Stragidex\Emelem\Authentication;
use Stragidex\Emelem\Codes;
use \WP_REST_Response;
use \WP_Error;

class Trees {
    static $_instance;
    public $codes;

    public function __construct ()
    {
        self::$_instance =& $this;
        $this->codes =& Codes::get_instance();
    }

    public static function create_unilevel ($user) {
        global $wpdb;

        $wpdb->insert('eee_binary',
            [
                'user_id' => $user->ID,
                'unilevel' => $user->ID,
                'position' => 0,
                'level' => 0,
                'parent' => 0
            ],
            ['%d', '%d', '%d', '%d', '%d']
        );
    }

    public static function create_branch ($args) {
        global $wpdb;

        $wpdb->insert('eee_binary',
            [
                'user_id' => $args->user_id,
                'unilevel' => $args->unilevel,
                'position' => $args->position,
                'level' => $args->level,
                'parent' => $args->parent
            ],
            ['%d', '%d', '%d', '%d', '%d']
        );
    }

    public function get_binary_tree ($request, $position = true)
    {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $current_logged_id = $authentication->get_id();
        if (method_exists($request, 'get_param')) {
            $user_id = $request->get_param('user_id') ?: $current_logged_id;
        } elseif (is_numeric($request)) {
            $user_id = $request;
        }

        // $unilevel = $request->get_param('unilevel');
        // $position = $request->get_param('position');

        // if ($unilevel) {
        //     $user_id = $this->get_user_id($unilevel, $position);
        // }

        $top = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT
                u.ID AS user_id, unilevel as node_unilevel, position as node_position, level as node_level, parent as node_parent, code,
                u.user_parent_id AS parent_id, u.user_referrer_id AS referrer_id,
                u.user_login AS username, u.user_email AS email,
                CONCAT(u.user_firstname, ' ', u.user_lastname) AS name,
                u.user_activated AS activated_at
                FROM eee_binary AS b
                LEFT JOIN eee_users AS u ON b.user_id = u.ID
                LEFT JOIN eee_codes AS c ON b.user_id = c.user_id
                WHERE b.user_id = %d",
                $user_id
            )
        );

        if (!$top) return;

        // $lines = $wpdb->get_results(
        //     $wpdb->prepare(
        //         "SELECT
        //         user_id, unilevel as node_unilevel, position as node_position, level as node_level, parent as node_parent, code,
        //         user_parent_id AS parent_id, user_referrer_id AS referrer_id,
        //         user_login AS username, user_email AS email,
        //         CONCAT(user_firstname, ' ', user_lastname) AS name,
        //         user_activated AS activated_at, @pv:=CONCAT(@pv, ',', `user_id`) AS `ancestors`
        //         FROM (
        //             SELECT b.*, u.*, c.code FROM eee_binary AS b
        //             LEFT JOIN eee_users AS u ON b.user_id = u.ID
        //             LEFT JOIN eee_codes AS c ON b.user_id = c.user_id
        //             WHERE unilevel = %d AND level >= %d
        //             ORDER BY unilevel, level, position
        //         ) AS binary_sorted,
        //         (SELECT @pv := %d) AS binary_init
        //         WHERE find_in_set(parent, @pv)",
        //         $top->node_unilevel, $top->node_level, $top->user_id
        //     )
        // );

        // MariaDB version

        // Fix for incorrect leg match
        $order = $position ? 'unilevel, level, position, activated_at' : 'unilevel, activated_at';
        $lines = $wpdb->get_results(
            $wpdb->prepare(
                "WITH RECURSIVE children AS (
                    SELECT b.*, u.*, c.code FROM eee_binary AS b
                    JOIN eee_users AS u ON b.user_id = u.ID
                    JOIN eee_codes AS c ON c.user_id = b.user_id
                    WHERE b.parent = %d AND b.unilevel = %d AND b.level > %d
                    UNION ALL
                    SELECT b.*, u.*, c.code
                    FROM eee_binary AS b
                    JOIN eee_users AS u ON b.user_id = u.ID
                    JOIN eee_codes AS c ON c.user_id = b.user_id
                    JOIN children ON children.user_id = b.parent
                ) SELECT user_id, unilevel as node_unilevel, position as node_position, level as node_level,
                parent as node_parent, code, user_parent_id AS parent_id, user_referrer_id AS referrer_id,
                user_login AS username, user_email AS email, CONCAT(user_firstname, ' ', user_lastname) AS name,
                user_activated AS activated_at
                FROM children ORDER BY $order",
                $top->user_id, $top->node_unilevel, $top->node_level
            )
        );

        array_unshift($lines, $top);
        return $lines;
    }

    public function get_downlines($request)
    {
        global $wpdb;

        $lines = $this->get_binary_tree($request);
        return $this->process_binary_tree($lines);
    }

    public function get_legs_count($request, $return = false)
    {
        $descendants = $this->get_direct_descendants($request);

        $legs = new \stdClass;

        if ($return) {
            $legs->left = array();
            $legs->right = array();
        } else {
            $legs->left = 0;
            $legs->right = 0;
        }

        foreach($descendants as $position => $descendant) {
            if ($descendant) {
                $lines = $this->get_binary_tree($descendant);
                // replace $lines fetch to correct the match making rewards
                // $lines = $this->get_binary_tree($descendant, false);
                if ($lines) {
                    if ($position > 0) {
                        $legs->right = $return ? $lines  : count($lines);
                    } else {
                        $legs->left = $return ? $lines : count($lines);
                    }
                }
            }
        }

        if ($return) {
            if (!is_array($legs->right)) $legs->right = [];
            if (!is_array($legs->left)) $legs->left = [];
        }

        return $legs;
    }

    public function get_direct_descendants($request)
    {
        global $wpdb;
        $authentication =& Authentication::get_instance();
        $user_id = is_numeric($request) ? $request : $authentication->get_id();

        $results = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT user_id, position FROM eee_binary WHERE parent = %d",
                $user_id
            )
        );

        $descendants = array('','');
        foreach($results as $descendant) {
            if ($descendant->position > 1) {
                $descendants[1] = $descendant->user_id;
            } else {
                $descendants[0] = $descendant->user_id;
            }
        }

        return $descendants;
    }

    private function process_binary_tree (&$lines) {
        $btree = array();
        $btree[0][0] = $lines[0];
        unset($lines[0]);
        $multiplier = 2;

        while(count($btree) <= 5) {
            $this->seek_children($btree, $lines, $multiplier);
            $multiplier *= 2;
        }

        return $btree;
    }

    private function seek_children(&$btree, &$lines, $multiplier)
    {
        switch($multiplier) {
            case 2:
                $level = 0;
                break;
            case 4:
                $level = 1;
                break;
            case 8:
                $level = 2;
                break;
            case 16:
                $level = 3;
                break;
            case 32:
                $level = 4;
                break;
            default:
                $level = 0;
            break;
        }

        while($multiplier > 0) {
            $children[] = (object) array(
                'available'     => false,
                'node_unilevel' => $btree[0][0]->node_unilevel,
                'node_position' => ($multiplier%2 === 0) ? 1 : 2,
                'node_level'    => '',
                'node_parent'   => '',
                'user_id'       => ''
            );
            $multiplier--;
        }

        $btree[] = $children;
        foreach($btree[$level] as $index => $node) {
            $btree[$level+1][$index*2]->node_parent = $node->user_id;
            $btree[$level+1][($index*2)+1]->node_parent = $node->user_id;

            $btree[$level+1][$index*2]->available = $btree[$level+1][$index*2]->node_parent ? true : false;
            $btree[$level+1][($index*2)+1]->available = $btree[$level+1][($index*2)+1]->node_parent ? true : false;

            $btree[$level+1][$index*2]->node_level = $btree[$level][0]->node_level + 1;
            $btree[$level+1][($index*2)+1]->node_level = $btree[$level][0]->node_level + 1;

            foreach($lines as $key => $line) {
                if (absint($node->user_id) === absint($line->node_parent)) {
                    $position = $line->node_position > 1 ? ($index*2)+1 : $index*2;
                    $btree[$level+1][$position] = $line;
                    $btree[$level+1][$position]->available = false;
                    unset($lines[$key]);
                }
            }
        }
    }

    private function get_user_id ($unilevel, $position) {
        global $wpdb;

        return $wpdb->get_var($wpdb->prepare("SELECT user_id FROM eee_binary WHERE unilevel = %d AND old_position = %d", $unilevel, $position));
    }

    public function reprocess_binary_tree($request)
    {
        global $wpdb;

        // return $wpdb->get_results("SELECT COUNT(user_id), parent FROM eee_binary GROUP BY parent");

        $unilevels = $wpdb->get_col("SELECT unilevel FROM eee_binary GROUP BY unilevel");

        if (!array_key_exists($request->get_param('unilevel') - 1, $unilevels)) return 'End of unilevels';
        $unilevel = $unilevels[$request->get_param('unilevel') - 1];

        $total = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT COUNT(user_id) FROM eee_binary WHERE unilevel = %d",
                $unilevel
            )
        );

        $lines = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM eee_binary WHERE unilevel = %d",
                $unilevel
            )
        );

        if (!$lines) return;

        $limit = 500;
        $pages = array_chunk($lines, $limit);
        $page = absint($request->get_param('page')) ?: 1;
        $page = $page - 1;

        if (!array_key_exists($page, $pages)) {
            $page = count($pages) - 1;
        } else {
            foreach($pages[$page] as $line) {
                if (absint($line->parent_node) === 0 && absint($line->old_position) !== 0) {
                    $update = $wpdb->update('eee_binary',
                        array('position' => 0),
                        array('user_id' => $line->user_id)
                    );
                } else {
                    $parent = $this->search_parent($lines, $line);
                    $position = (absint($line->old_position) > (absint($parent->old_position) * 2)) ? 2 : 1;
                    if (!$position) return $line;
                    $update = $wpdb->update('eee_binary',
                        array('position' => $position, 'parent' => $parent->user_id),
                        array('user_id' => $line->user_id)
                    );
                }
            }
        }

        return new WP_REST_Response(['unilevel' => absint($unilevel), 'page' =>  $page + 1, 'total' => count($pages)], 200);
    }

    private function search_parent(&$lines, $node) {
        foreach($lines as $line) {
            if (absint($line->old_position) === absint($node->parent_node)) return $line;
        }

        return new WP_Error($node->parent_node);
    }

    public static function &get_instance () {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
