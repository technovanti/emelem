<?php
namespace Stragidex\Models;
use Stragidex\Emelem\Authentication;
use Stragidex\Models\Users;
use Stragidex\Models\Rewards;
use \WP_REST_Response;
use \WP_Error;

class Encashments {
	static $_instance;

	public function __construct ()
	{
        self::$_instance =& $this;
	}

    public function get_encashments ($request)
    {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();
        if (method_exists($request, 'get_param')) {
            $user_id = $request->get_param('user_id') ?: $authentication->get_id();
        }

        if (!$user_id) {
            return new WP_Error(
                'username_inavalid',
                __('Invalid username', 'emelem'),
                array(
                    'status' => 403
                )
            );
        }

        $encashments = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT amount, type, status, created_at, updated_at, deleted_at FROM eee_encashments AS e
                JOIN eee_encashment_details AS d ON d.encashment_id = e.encashment_id
                WHERE user_id = %d AND deleted_at = %s ORDER BY created_at DESC, status ASC",
                $user_id, '0000-00-00 00:00:00'
            )
        );

        return new WP_REST_Response($encashments, 200);
    }

    public function add_encashment ($request)
    {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();

        if (!$user_id) {
            return new WP_Error(
                'username_inavalid',
                __('Invalid username', 'emelem'),
                array(
                    'status' => 403
                )
            );
        }

        $rewards =& Rewards::get_instance();
        $rewards->get_rewards($user_id);
        $available = $rewards->available['total'];
        if ($amount > $available) {
            return new WP_Error(
                'invalid_amount',
                __('Invalid amount', 'emelem'),
                array(
                    'status' => 403
                )
            );
        }

        $date = current_time('mysql');

        $encashment = array(
            'user_id' => $user_id,
            'amount' => $request->get_param('amount'),
            'status' => 0,
            'created_at' => $date,
            'updated_at' => $date
        );

        $wpdb->insert('eee_encashments', $encashment);
        $encashment_id = $wpdb->insert_id;

        $details = array(
            'encashment_id' => $encashment_id,
            'type' => $request->get_param('type'),
            'name' => $request->get_param('name'),
            'account' => $request->get_param('account'),
            'branch' => $request->get_param('branch')
        );
        $wpdb->insert('eee_encashment_details', $details);

        return new WP_REST_Response($encashment_id , 200);
    }

	public static function &get_instance () {
		if (!isset(self::$_instance)) self::$_instance = new self;

		return self::$_instance;
	}
}
