<?php
namespace Stragidex\Models;
use Stragidex\Emelem\Authentication;
use Stragidex\Models\Users;
use Stragidex\Models\Rewards;
use \WP_REST_Response;
use \WP_Error;

class Admin {
	static $_instance;

	public function __construct ()
	{
        self::$_instance =& $this;
        $this->users =& Users::get_instance();
	}

    public function get_encashments ($request)
    {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $admin = $this->users->get_user($authentication->get_id());

        if ($admin->user_role < 2) {
            return new WP_Error(
                'admin_username_inavalid',
                __('Invalid admin username', 'emelem'),
                array(
                    'status' => 403
                )
            );
        }

        $encashments = $wpdb->get_results(
          "SELECT e.encashment_id, user_login, ID, amount, type, status, created_at, updated_at, deleted_at FROM eee_encashments AS e
          JOIN eee_encashment_details AS d ON d.encashment_id = e.encashment_id
          LEFT JOIN eee_users AS u ON u.ID = e.user_id
          ORDER BY created_at DESC"
        );

        foreach($encashments as $key => $encashment) {
            if (absint($encashment->status) === 0) {
                $rewards = new Rewards();
                $reward = $rewards->get_rewards($encashment->ID);

                $encashments[$key]->available = ($reward->rewards['total'] - $reward->encashment);
                $encashments[$key]->encashments = $reward->encashment;
            }
        }

        return new WP_REST_Response($encashments, 200);
    }

    public function edit_encashments ($request) {
      global $wpdb;
      $encashment = $request->get_params();

      $authentication =& Authentication::get_instance();
      $admin = $this->users->get_user($authentication->get_id());

      if ($admin->user_role < 2) {
          return new WP_Error(
              'admin_username_inavalid',
              __('Invalid admin username', 'emelem'),
              array(
                  'status' => 403
              )
          );
      }

      $date = current_time('mysql');

      $update = $wpdb->update(
        'eee_encashments',
        array(
          'status' => $encashment['status'],
          'updated_at' => $date
        ),
        array('encashment_id' => $encashment['encashment_id'])
      );

      if ($update) {
        $action = $encashment['status'] === 0 ? 'pending' : 'approved';
        $action = $encashment['status'] > 0 && $encashment['status'] < 2 ? 'approved' : $action;
        $action = $encashment['status'] > 1 ? 'paid' : $action;

        $wpdb->insert(
          'eee_encashmenthistory',
          array(
            'encashment_id' => $encashment['encashment_id'],
            'encashment_action' => $action,
            'user_id' => $authentication->get_id(),
            'history_date' => $date
          )
        );

        return new WP_REST_Response($encashment, 200);
      }

      return new WP_REST_Response([], 404);
    }

    public function delete_encashments ($request) {
      global $wpdb;
      $encashment = $request->get_param('item');

      $authentication =& Authentication::get_instance();
      $admin = $this->users->get_user($authentication->get_id());

      if ($admin->user_role < 2) {
          return new WP_Error(
              'admin_username_inavalid',
              __('Invalid admin username', 'emelem'),
              array(
                  'status' => 403
              )
          );
      }

      $date = current_time('mysql');

      $delete = $wpdb->update(
        'eee_encashments',
        array(
          'status' => 3,
          'updated_at' => $date,
          'deleted_at' => $date
        ),
        array(
          'encashment_id' => $encashment['encashment_id']
        )
      );

      if ($delete) {
        $wpdb->insert(
          'eee_encashmenthistory',
          array(
            'encashment_id' => $encashment['encashment_id'],
            'encashment_action' => 'rejected',
            'user_id' => $authentication->get_id(),
            'history_date' => $date
          )
        );

        $encashment['deleted_at'] = $date;
        return new WP_REST_Response($encashment, 200);
      }

      return new WP_REST_Response([], 404);
    }

    public function get_encashment_dates ($request)
    {
        global $wpdb;

        $dates = $wpdb->get_col(
            "SELECT date_format(created_at, '%Y-%m-%d') FROM eee_encashments GROUP BY date_format(created_at, '%Y-%m-%d')"
        );

        return new WP_REST_Response($dates, 200);
    }

    public function get_rewards_report ($request) {
      global $wpdb;

      $user_id = $request->get_param('user_id');
      if (!is_numeric($user_id)) {
        $user_id = $wpdb->get_var(
          $wpdb->prepare(
            "SELECT ID FROM eee_users WHERE user_login = %s",
            $user_id
          )
        );
      }

      if (!$user_id) return;
      $user_id = absint($user_id);

      $rewards = new Rewards();
      $reward = $rewards->get_rewards($user_id);

      return $reward;
    }

    public static function any_method($request)
    {
        //
    }

	public static function &get_instance () {
		if (!isset(self::$_instance)) self::$_instance = new self;

		return self::$_instance;
	}
}
