<?php
namespace Stragidex\Models;
use Stragidex\Emelem\Authentication;
use Stragidex\Models\Users;
use Stragidex\Models\Trees;
use \WP_REST_Response;
use \WP_Error;

class Rewards {
    static $_instance;
    static $_user_id;
    protected $user;
    protected $users;
    protected $is_active = false;
    protected $activated_at;
    protected $topmax;
    protected $old;

    public $rewards = [
        'signup' => [
            'rewards' => [],
            'total' => 0
        ],
        'youtube' => [
            'rewards' => [],
            'total' => 0
        ],
        'customer_referral' => [
            'rewards' => [],
            'total' => 0
        ],
        'leadership_matching' => [
            'rewards' => [],
            'total' => 0
        ], // 200 rewards on the weakest
        'topmax' => [
            'rewards' => [],
            'total' => 0
        ],
        'total' => 0,
    ];

    public $available = [
        'signup' => 0,
        'youtube' => 0,
        'customer_referral' => 0,
        'leadership_matching' => 0, // 200 rewards on the weakest
        'topmax' => 0,
        'total' => 0
    ];

    public $flushouts = [
        'customer_referral' => [
            'flushouts' => [],
            'total' => 0
        ],
        'leadership_matching' => [
            'flushouts' => [],
            'total' => 0
        ],
        'topmax' => [
            'flushouts' => [],
            'total' => 0
        ],
        'total' => 0
    ];

    public $shares = [
        'topmax' => [
            'rewards' => [],
            'total' => 0
        ]
    ];

    public $matchmakingByDate = [];
    public $topmaxByDate = [];

    public $encashment = 0;

    protected $safetynet;

    public function __construct ($user_id = null, $topmax = true)
    {
        self::$_instance =& $this;

        $this->topmax =  $topmax;
        $this->safetynet = 3000;
        if ($user_id) {

            $this->users =& Users::get_instance();
            $this->user = $this->users->get_user($user_id);

            if (!$this->user) return new WP_Error(
                'rewards_invalid_user',
                __('No user found.'),
                ['status' => 403]
            );

            unset($this->user->user_pass);
            unset($this->user->ip);
            unset($this->user->user_role);
            unset($this->user->user_activation_key);

            $this->is_active = ($this->user->user_activated !== '0000-00-00 00:00:00');
            $this->registered_at = date('Y-m-d', strtotime($this->user->user_registered));
            $this->activated_at = date('Y-m-d', strtotime($this->user->user_activated));

        }
    }

    public function generate () {
        $this->has_old($this->user->ID);
        $this->signup();
        $this->youtube();
        $this->customer_referral();
        $this->pairing(); // Leadership Matching
        if ($this->topmax) {
            $this->topmax();
            $this->encashments();
        }
    }

    private function signup () {
        $this->rewards['signup']['rewards'][$this->registered_at] = 500;
        $this->rewards['signup']['total'] += 500;
        $this->rewards['total'] += 500;

        if ($this->is_active) {
            $this->available['signup'] += 500;
            $this->available['total'] += 500;
        }
    }

    private function youtube () {

        // Instant watch video rewards below 2019-09-11
        if (strtotime($this->activated_at) <= strtotime('2019-09-11')) {
            $this->rewards['youtube']['rewards'][$this->registered_at] = 0;
            for($i=0;$i<5;$i++) {
                $this->rewards['youtube']['rewards'][$this->registered_at] += 300;
                $this->rewards['youtube']['total'] += 300;
                $this->rewards['total'] += 300;

                if ($this->is_active) {
                    $this->available['youtube'] += 300;
                    $this->available['total'] += 300;
                }
            }
        } else {
            $videos = $this->get_watchvideos($this->user->ID);
            if ($videos) {
                foreach($videos as $watch) {
                    $amount = $watch->amount;

                    // If total is equal 1500 then break
                    if ($this->rewards['youtube']['total'] == 1500) break;

                    // Lets get the lacking amount to make it 1500
                    if( ( $this->rewards['youtube']['total'] + $watch->amount ) > 1500 )
                        $amount = ( $this->rewards['youtube']['total'] + $watch->amount ) - 1500;

                    if ($watch->amount > 0 ) {
                        $this->rewards['youtube']['rewards'][$watch->day] = (double) $amount;
                        $this->rewards['youtube']['total'] += $amount;
                        $this->rewards['total'] += $amount;

                        if ($this->is_active) {
                            $this->available['youtube'] += $amount;
                            $this->available['total'] += $amount;
                        }
                    }
                }
            }
        }
    }

    private function customer_referral () {
        if (!$this->is_active) return;

        $referrals = $this->get_daily_referrals($this->user->ID);

        if (!$referrals) return;
        foreach($referrals as $referral) {
            $current = 0;
            if (isset($this->rewards['signup']['rewards'][$referral->day])) {
                $current += $this->rewards['signup']['rewards'][$referral->day];
            }

            if (isset($this->rewards['youtube']['rewards'][$referral->day])) {
                $current += $this->rewards['signup']['rewards'][$referral->day];
            }

            if (($referral->amount + $current) > 3000) {
                $flushout = (($referral->amount + $current) - 3000);
                $referral->amount = $referral->amount - $flushout;
            }

            $this->rewards['customer_referral']['rewards'][$referral->day] = (double) $referral->amount;
            $this->rewards['customer_referral']['total'] += $referral->amount;
            $this->rewards['total'] += $referral->amount;

            if (isset($flushout)) {
                $this->flushouts['customer_referral']['flushouts'][$referral->day] = (double) $flushout;
                $this->flushouts['customer_referral']['total'] += $flushout;
                $this->flushouts['total'] += $flushout;
                unset($flushout);
            }

            $this->available['customer_referral'] += $referral->amount;
            $this->available['total'] += $referral->amount;
        }

        if ($this->old->customer_referral->total < 1 ) return;

        if ($this->old && $this->old->customer_referral->total > $this->rewards['customer_referral']['total']) {
            $lost = $this->old->customer_referral->total - $this->rewards['customer_referral']['total'];

            if ($this->flushouts['customer_referral']['total'] > $lost) {
                $flushout = $this->flushouts['customer_referral']['total'] - $lost;
                $this->flushouts['customer_referral']['total'] -= $lost;
                $this->flushouts['total'] -= $lost;
            }

            $this->rewards['customer_referral']['total'] += $lost;
            $this->rewards['total'] += $lost;
            $this->available['customer_referral'] += $lost;
            $this->available['total'] += $lost;
        } elseif ($this->old && $this->old->customer_referral->total > 0) {
            error_log($this->user->ID . ' Has lower old customer_referral rewards from new.');
            // what if $this->old is less than
            // $lost = $this->rewards['leadership_matching']['total'] - $this->old->leadership_matching->total;
        }
    }

    private function pairing () {
        if (!$this->is_active) return;

        $trees =& Trees::get_instance();
        $legs = $trees->get_legs_count($this->user->ID, true);

        $low = count($legs->left) >= count($legs->right) ? $legs->right : $legs->left;
        $high = count($legs->left) >= count($legs->right) ? $legs->left : $legs->right;

        $pairing = array();
        if ($low > 0) {
            foreach($low as $key => $left) {
                $right =& $high[$key];
                if (strtotime($left->activated_at) >= strtotime($right->activated_at)) {
                     $day = date('Y-m-d', strtotime($left->activated_at));
                 } else {
                     $day = date('Y-m-d', strtotime($right->activated_at));
                 }

                if (!isset($pairing[$day])) $pairing[$day] = 0;

                $this->matchmakingByDate[$day][] = array(
                  'left' => $left,
                  'right' => $right
                );

                $pairing[$day] += 200;
            }

            ksort($this->matchmakingByDate);
        }

        foreach($pairing as $day => $pairs) {
            $current = 0;
            if (isset($this->rewards['customer_referral']['rewards'][$day])) {
                $current += $this->rewards['customer_referral']['rewards'][$day];
            }

            if (($pairs + $current) > 3000) {
                $flushout = (($pairs + $current) - 3000);
                $pairs = $pairs - $flushout;
            }

            $share = $pairs * 0.1;

            $this->rewards['leadership_matching']['rewards'][$day] = $pairs;
            $this->rewards['leadership_matching']['total'] += $pairs;
            $this->rewards['total'] += $pairs;
            $this->shares['topmax']['rewards'][$day] = $share;
            $this->shares['topmax']['total'] += $share;

            if (isset($flushout)) {
                $this->flushouts['leadership_matching']['flushouts'][$day] = $flushout;
                $this->flushouts['leadership_matching']['total'] += $flushout;
                $this->flushouts['total'] += $flushout;
                unset($flushout);
            }

            $this->available['leadership_matching'] += $pairs;
            $this->available['total'] += $pairs;
        }

        if ($this->old->leadership_matching->total < 1) return;

        if ($this->old && $this->old->leadership_matching->total > $this->rewards['leadership_matching']['total']) {
            $lost = $this->old->leadership_matching->total - $this->rewards['leadership_matching']['total'];

            if ($this->flushouts['leadership_matching']['total'] > $lost) {
                $flushout = $this->flushouts['leadership_matching']['total'] - $lost;
                $this->flushouts['leadership_matching']['total'] -= $lost;
                $this->flushouts['total'] -= $lost;
            }

            $this->rewards['leadership_matching']['total'] += $lost;
            $this->rewards['total'] += $lost;
            $this->available['leadership_matching'] += $lost;
            $this->available['total'] += $lost;
        } elseif ($this->old && $this->old->leadership_matching->total > 0) {
            error_log($this->user->ID . ' Has lower old matching rewards from new.');
            // what if $this->old is less than
            // $lost = $this->rewards['leadership_matching']['total'] - $this->old->leadership_matching->total;
        }
    }

    private function topmax () {
        global $wpdb;
        $parent = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT parent FROM eee_binary WHERE user_id = %d",
                $this->user->ID
            )
        );

        $rewards = new self($parent, false);
        $rewards->generate();

        $this->topmaxByDate = $rewards->matchmakingByDate;

        if ($rewards->shares['topmax']['total'] < 1) return;

        foreach($rewards->shares['topmax']['rewards'] as $day => $topmax) {
            $current = 0;
            if (isset($this->rewards['customer_referral']['rewards'][$day])) {
                $current += $this->rewards['customer_referral']['rewards'][$day];
            }

            if (isset($this->rewards['leadership_matching']['rewards'][$day])) {
                $current += $this->rewards['leadership_matching']['rewards'][$day];
            }

            if (($topmax + $current) > 3000) {
                $flushout = (($topmax + $current) - 3000);
                $topmax = $topmax - $flushout;
            }

            $this->rewards['topmax']['rewards'][$day] = $topmax;
            $this->rewards['topmax']['total'] += $topmax;
            $this->rewards['total'] += $topmax;

            if (isset($flushout)) {
                $this->flushouts['topmax']['flushouts'][$day] = $flushout;
                $this->flushouts['topmax']['total'] += $flushout;
                $this->flushouts['total'] += $flushout;
                unset($flushout);
            }

            $this->available['topmax'] += $topmax;
            $this->available['total'] += $topmax;
        }
    }

    private function encashments ()
    {
        global $wpdb;

        $this->encashment = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT
                SUM(amount) AS amount
                FROM eee_encashments WHERE user_id = %d AND deleted_at = %s",
                $this->user->ID, '0000-00-00 00:00:00'
            )
        ) ?: 0;

        $total = $this->encashment;
        if ($total > 0) {
            foreach($this->available as $reward => $available) {
                if ($reward === 'total') continue;

                if ($total > $available) {
                    $this->available[$reward] = 0;
                    $this->available['total'] -= $available;
                    $total = $total - $available;
                } else {
                    $this->available[$reward] = ($available - $total);
                    $this->available['total'] -= $total;
                    break;
                }
            }
        }
    }

    private function has_old($user_id)
    {
        global $wpdb;

        $old_rewards = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT rewards FROM eee_old_rewards WHERE user_id = %d",
                $user_id
            )
        );

        if (!$old_rewards) return;

        $this->old = json_decode($old_rewards);
    }

    private function pairing_before_adjustment ($pairing)
    {
        foreach($this->old->leadership_matching->rewards as $day => $pairs) {
            $flushout = 0;
            if (isset($this->old->leadership_matching->flushouts->{$day})) {
                $flushout = $this->old->leadership_matching->flushouts->{$day};
            }

            $pairing[$day] = $pairs + $flushout;
        }

        return $pairing;
    }

    public function get_watchvideos ($user_id) {
        global $wpdb;

        return $wpdb->get_results(
            $wpdb->prepare(
                "SELECT
                SUM(amount) AS amount, date_format(date_added, '%%Y-%%m-%%d') AS day
                FROM eee_watches
                WHERE user_id = %d
                GROUP BY date_format(date_added, '%%Y-%%m-%%d')",
                $user_id
            )
        );
    }

    public function add_watchvideo ($request)
    {
        return new WP_Error('watch_video_failed', __('Temporary disabled by admin. Please try contact your admin.', 'emelem'), ['status' => 403]);
    }

    public function get_watchvideo ($request)
    {
        return new WP_Error('watch_video_failed', __('Temporary disabled by admin. Please try contact your admin.', 'emelem'), ['status' => 403]);
    }

    public function get_daily_referrals ($user_id) {
        global $wpdb;

        return $wpdb->get_results(
            $wpdb->prepare(
                "SELECT
                SUM(100) AS amount, date_format(user_activated, '%%Y-%%m-%%d') AS day
                FROM eee_users AS u
                LEFT JOIN eee_codes AS b ON u.ID = b.user_id
                WHERE user_referrer_id = %d AND user_activated != %s
                GROUP BY date_format(user_activated, '%%Y-%%m-%%d')",
                $user_id, '0000-00-00 00:00:00'
            )
        );
    }

    public static function get_available_rewards ($request) {
        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();
        if (!$user_id) return;

        $rewards = new self($user_id);

        return $rewards->available;
    }

    public function get_rewards ($request, $topmax = true) {
        $authentication =& Authentication::get_instance();
        if (method_exists($request, 'get_param')) {
            $user_id = $request->get_param('user_id') ?: $authentication->get_id();
        } elseif (is_numeric($request)) {
            $user_id = $request;
        }

        if (!$user_id) return;

        $rewards = new self($user_id, $topmax);
        $rewards->generate();
        return $rewards;
    }

    public static function &get_instance ()
    {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
