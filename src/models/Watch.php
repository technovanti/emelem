<?php
namespace Stragidex\Models;
use Stragidex\Emelem\Authentication;
use \WP_REST_Response;
use \WP_Error;

class Watch {
    static $_instance;

    public function __construct ()
    {
        self::$_instance =& $this;
    }

    public function get_watch ($request)
    {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        if (method_exists($request, 'get_param')) {
            $user_id = $request->get_param('user_id') ?: $authentication->get_id();
        } elseif (is_numeric($request)) {
            $user_id = $request;
        }
        $videoId = $this->get_video($user_id);

        if ($authentication->get_activated_at() === '0000-00-00 00:00:00') {
            return new WP_REST_Response(['watched' => true, 'total' => 0, 'videoId' => $videoId], 200);
        }

        $today = $this->watched_today($user_id);
        $watched = $today ? true : false;

        if (strtotime($authentication->get_activated_at()) <= strtotime('2019-09-11')) {
            $total = 1500;
        } else {
            $total = $wpdb->get_var(
                $wpdb->prepare(
                    "SELECT if(SUM(amount)>1500, 1500, SUM(amount)) AS amount FROM eee_watches WHERE user_id = %d",
                    $user_id
                )
            ) ?: 0;
        }

        if ($total >= 1500) $watched = true;

        return new WP_REST_Response(['watched' => $watched, 'total' => $total, 'videoId' => $videoId], 200);
    }

    public function add_watch ($request)
    {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();

        $watched = $this->watched_today($user_id);
        if (strtotime($authentication->get_activated_at()) <= strtotime('2019-09-11')) {
            $watched = true;
        }

        if (!$watched) {
            $youtube_player = $request->get_param('youtube');

            foreach( $youtube_player as $player ) {
                if (isset($player['videoData'])){
                    $youtube = $player;
                    break;
                }
            }

            if( !isset( $youtube ) ) return;
            unset($youtube['apiInterface']);

            $youtube_id = $youtube['videoData']['video_id'];
            $progress = round($youtube['currentTime']);
            $duration = round($youtube['duration']);

            if ($duration < 60 && $duration > $progress) {
                return new WP_Error(
                    'no_watch_hack',
                    'You are trying to find bugs in watched video. This is recorded.',
                    array(
                        'status' => 403
                    )
                );
            }

            if ($duration >= 60 && $progress < 60) {
                return new WP_Error(
                    'no_watch_hack',
                    'You are trying to find bugs in watched video. This is recorded.',
                    array(
                        'status' => 403
                    )
                );
            }

            $add = $wpdb->insert('eee_watches', array(
                'user_id' => $user_id,
                'youtube_id' => $youtube_id,
                'amount' => 300,
                'date_added' => current_time('mysql')
            ));

            return new WP_REST_Response([$add], 200);
        }

        return new WP_REST_Response([], 200);
    }

    private function watched_today ($user_id)
    {
        global $wpdb;

        $last = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT date_format(date_added, '%%Y-%%m-%%d') as day FROM eee_watches WHERE user_id = %d ORDER BY date_added DESC",
                $user_id
            )
        );

        if (!$last) return false;

        if ( date('Y-m-d', strtotime($last)) === date('Y-m-d', strtotime(current_time('mysql'))) ) return true;

        return false;
    }

    private function get_video ($user_id)
    {
        global $wpdb;

        $last = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT youtube_id FROM eee_watches WHERE user_id = %d",
                $user_id
            )
        );

        if ($last) {
            return $wpdb->get_var(
                $wpdb->prepare(
                    "SELECT youtube_id FROM eee_videos WHERE youtube_id != %s ORDER BY RAND() LIMIT 1",
                    $last
                )
            );
        } else {
            return $wpdb->get_var("SELECT youtube_id FROM eee_videos ORDER BY RAND() LIMIT 1");
        }
    }

    public static function &get_instance () {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
