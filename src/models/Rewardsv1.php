<?php
namespace Stragidex\Models;
use Stragidex\Emelem\Authentication;
use Stragidex\Models\Encashments;
use Stragidex\Models\Users;
use \WP_REST_Response;
use \WP_Error;

class Rewardsv1 {
    static $_instance;
    protected $user;
    protected $is_active = false;
    protected $activated_at;

    protected $rewards = [
        'signup' => [
            'rewards' => [],
            'flushouts' => [],
            'flushout' => 0,
            'total' => 0
        ],
        'youtube' => [
            'rewards' => [],
            'flushouts' => [],
            'flushout' => 0,
            'total' => 0
        ],
        'customer_referral' => [
            'rewards' => [],
            'flushouts' => [],
            'flushout' => 0,
            'total' => 0
        ],
        'leadership_matching' => [
            'rewards' => [],
            'flushouts' => [],
            'flushout' => 0,
            'total' => 0
        ], // 200 rewards on the weakest
        'topmax' => [
            'rewards' => [],
            'flushouts' => [],
            'flushout' => 0,
            'total' => 0
        ],
        'flushouts' => [],
        'flushout' => 0,
        'total' => 0,
    ];

    protected $available = [
        'signup' => 0,
        'youtube' => 0,
        'customer_referral' => 0,
        'leadership_matching' => 0, // 200 rewards on the weakest
        'topmax' => 0,
        'encashment' => 0,
        'total' => 0
    ];

    public $shares = [
        'topmax' => [
            'rewards' => [],
            'flushouts' => [],
            'flushout' => 0,
            'total' => 0
        ]
    ];

    protected $encashments = [
        'total' => 0
    ];

    protected $safetynet;

    public function __construct ($user_id = null, $all = true)
    {
        // $user_id = 15;
        $this->safetynet = 3000;

        if (!$user_id) return new WP_Error(
            'rewards_invalid_user',
            __('No user.'),
            ['status' => 403]
        );

        $this->users =& Users::get_instance();
        $this->user = $this->users->get_user($user_id);

        if (!$this->user) return new WP_Error(
            'rewards_invalid_user',
            __('No user found.'),
            ['status' => 403]
        );

        unset($this->user->user_pass);
        unset($this->user->ip);
        unset($this->user->user_role);
        unset($this->user->user_activation_key);

        $this->is_active = ($this->user->user_activated !== '0000-00-00 00:00:00');
        $this->registered_at = date('Y-m-d', strtotime($this->user->user_registered));
        $this->activated_at = date('Y-m-d', strtotime($this->user->user_activated));

        if( $all )
            $this->generate();
    }

    private function generate () {
        $this->signup();
        $this->youtube();
        $this->customer_referral();
        $this->leadership_matching();
        $this->topmax();
        $this->encashments();

        // Process Daily Limit and Add to Flushouts
        $this->process_flushouts();
    }

    private function process_flushouts() {

        $daily_rewards = [];
        $daily_max_reward = 3000;
        if( $this->rewards['total'] > 0 ){
            foreach ( $this->rewards as $reward ) {

                if(is_array($reward['rewards']) && sizeof($reward['rewards']) > 0 ){
                    //sd( $reward['rewards'] );
                    foreach( $reward['rewards'] as $day => $amount ){
                        $daily_rewards[$day] = isset( $daily_rewards[$day] ) ? $daily_rewards[$day] + $amount :  $amount;

                        /* Flushouts - If greater than daily max reward */
                        if( $daily_rewards[$day] > $daily_max_reward ) {
                            $flushout = ( $daily_rewards[ $day ] - $daily_max_reward );
                            $daily_rewards[$day] = $daily_rewards[$day] - $daily_max_reward;
                            $this->rewards['flushouts'][$day] = isset( $this->rewards['flushouts'][$day] ) ?
                                $this->rewards['flushouts'][$day] + $flushout : $flushout;

                            $this->rewards['flushout'] += $flushout;
                        }

                    }
                }

            }
        }

        //$this->rewards['total'] = $this->rewards['total'] - $this->rewards['flushout'];
        $this->available['total'] = $this->available['total'] - $this->rewards['flushout'];
    }

    private function encashments() {
        $encashmentModel = new Encashments;
        //sd( $encashmentModel );
        $encashments = $encashmentModel->total_encashments_amount( $this->user->ID, 0, 0 );
        //sd( $encashments );
        $totalEncashments = isset( $encashments[0] ) ? $encashments[0] : 0;
        $this->available['encashment'] = $totalEncashments;
        $this->available['total'] = $this->available['total'] - $totalEncashments;
    }

    private function signup () {
        $this->rewards['signup']['rewards'][$this->registered_at] = 500;
        $this->rewards['signup']['total'] += 500;
        $this->rewards['total'] += 500;

        if ($this->is_active) {
            $this->available['signup'] += 500;
            $this->available['total'] += 500;
        }
    }

    private function youtube () {
        if (!$this->is_active) return;

        $max_watch_amount = 1500;
        if( strtotime( $this->activated_at ) <= strtotime( '2019-09-11' ) ) {

            $this->rewards['youtube']['rewards'][ $this->activated_at ] = $max_watch_amount;
            $this->rewards['youtube']['total'] = $max_watch_amount;
            $this->rewards['total'] += $max_watch_amount;

            $this->available['youtube'] += $max_watch_amount;
            $this->available['total'] += $max_watch_amount;
            return;
        }

        $watchvideo = $this->get_watchvideo($this->user->ID);

        if (!$watchvideo) return;
        foreach($watchvideo as $watch) {
            $amount = $watch->amount;
            if( ( $this->rewards['youtube']['total'] + $watch->amount ) > $max_watch_amount )
                $amount = $max_watch_amount - $this->rewards['youtube']['total'];

            $this->rewards['youtube']['rewards'][$watch->day] = absint($amount);
            $this->rewards['youtube']['total'] += $amount;
            $this->rewards['total'] += $amount;

            $this->available['youtube'] += $amount;
            $this->available['total'] += $amount;
        }
    }

    public function get_watchvideo ( $user_id )
    {
        global $wpdb;
        return $wpdb->get_results(
            "SELECT
            SUM(amount) AS amount, date_format(date_added, '%Y-%m-%d') AS day
            FROM eee_video_watch
            WHERE user_id = $user_id
            GROUP BY date_format(date_added, '%Y-%m-%d')"
        );
    }

    public function get_watchvideo_total ()
    {
        $this->youtube();
        return $this->available['youtube'];
    }

    public function add_watchvideo ($youtube)
    {
        global $wpdb;

        $max_watch_amount = 1500;
        $watch_amt = 300;
        // Disabled quickly for no directs
        $total = $this->get_watchvideo_total();
        if ( $total >= $max_watch_amount ) return $total;

        $videoId = $youtube['videoData']['video_id'];
        $progress = round($youtube['currentTime']);
        $duration = round($youtube['duration']);

        $watched = $this->watched_today();
        if ($watched && $watched->amount > 0) return $total;

        $last = $this->get_last_watchvideo_reward();

        if ($last && $watched) {
            if ($last->videoid !== $videoId) return $total;
            $watchId = $last->watch_id;
        } else {
            $watchId = $this->add_video_watch($videoId);
            return $total;
        }

        if ($duration < 60 && $duration > $progress) return $total;
        if ($duration >= 60 && $progress < 60) return $total;

        $this->add_video_watch($videoId, $watchId);

        return $total + $watch_amt;
    }

    public function watched_today ()
    {
        global $wpdb;

        $query = $wpdb->prepare(
            "SELECT * FROM eee_video_watch WHERE user_id = %d AND DATE(date_added) = %s",
            $this->user->ID,
            date('Y-m-d', strtotime(current_time('mysql')))
        );

        return $wpdb->get_row($query);
    }

    public function add_video_watch ($videoId, $watchId = false)
    {
        global $wpdb;
        $watch_amt = 300;
        if (!$watchId && !$this->watched_today()) {
            $watch = $wpdb->query(
                $wpdb->prepare(
                    "INSERT INTO eee_video_watch (user_id, videoid, amount) VALUES (%d, %s, %d)",
                    $this->user->ID,
                    $videoId,
                    $watch_amt
                )
            );
        } else {
            $watch = $wpdb->query(
                $wpdb->prepare(
                    "UPDATE eee_video_watch SET amount = %d WHERE watch_id = %d AND videoid = %s AND user_id = %d",
                    $watch_amt,
                    $watchId,
                    $videoId,
                    $this->user->ID
                )
            );
        }

        if ($watch) return $wpdb->insert_id;
    }

    public function get_last_watchvideo_reward ()
    {
        global $wpdb;

        return $wpdb->get_row(
            $wpdb->prepare(
                "SELECT watch_id, date_added AS created_at, videoid, amount FROM eee_video_watch
                WHERE user_id = %d ORDER BY date_added DESC LIMIT 1",
                $this->user->ID
            )
        );
    }

    private function customer_referral () {
        if (!$this->is_active) return;

        $referrals = $this->get_daily_referrals($this->user->ID);

        if (!$referrals) return;
        foreach($referrals as $referral) {
            if ($referral->total > 3000) {
                $flushout = ($referral->total - 3000);
                $referral->total = $referral->total - $flushout;
            }

            $this->rewards['customer_referral']['rewards'][$referral->day] = absint($referral->total);
            $this->rewards['customer_referral']['total'] += $referral->total;
            $this->rewards['total'] += $referral->total;

            if (isset($flushout)) {
                $this->rewards['customer_referral']['flushouts'][$referral->day] = $flushout;
                $this->rewards['customer_referral']['flushout'] += $flushout;
                //$this->rewards['flushout'] += $flushout;
            }

            $this->available['customer_referral'] += $referral->total;
            $this->available['total'] += $referral->total;
        }
    }

    public function get_daily_referrals ($user_id) {
        global $wpdb;
        $referral_amt = 100;
        return $wpdb->get_results(
            "SELECT
            SUM($referral_amt) AS total, date_format(user_activated, '%Y-%m-%d') AS day
            FROM eee_users AS u
            LEFT JOIN eee_codes AS b ON u.ID = b.user_id
            WHERE user_referrer_id = $user_id AND user_activated != '0000-00-00 00:00:00'
            GROUP BY date_format(user_activated, '%Y-%m-%d')"
        );
    }

    public function leadership_matching () {
        if (!$this->is_active) return;

        $matchings = $this->get_leadership_matching( $this->user->ID );
        if(!$matchings) return;


        $maxPairing = 3000;
        foreach($matchings as $matching) {
            if ( $matching->total == 0 ) continue;
            unset( $flushout );
            if ($matching->total > $maxPairing) {
                $flushout = ($matching->total - $maxPairing);
                $matching->total = $matching->total - $flushout;
            }

            $this->rewards['leadership_matching']['rewards'][$matching->day] = absint($matching->total);
            $this->rewards['leadership_matching']['total'] += $matching->total;
            $this->rewards['total'] += $matching->total;

            if (isset($flushout)) {
                $this->rewards['leadership_matching']['flushouts'][$matching->day] = $flushout;
                $this->rewards['leadership_matching']['flushout'] += $flushout;
                //$this->rewards['flushout'] += $flushout;
            }

            $this->available['leadership_matching'] += $matching->total;
            $this->available['total'] += $matching->total;
        }
    }

    public function get_leadership_matching ($user_id) {
        global $wpdb;

        $top = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT
                u.ID AS user_id, unilevel, position, level, nodes, node, parent_node, code,
                u.user_parent_id AS parent_id, u.user_referrer_id AS referrer_id,
                u.user_login AS username, u.user_email AS email, u.user_membership AS membership,
                CONCAT(u.user_firstname, ' ', u.user_lastname) AS name,
                u.user_activated AS activated_at
                FROM eee_binaryv1 AS b
                LEFT JOIN eee_users AS u ON b.user_id = u.ID
                LEFT JOIN eee_codes AS c ON b.user_id = c.user_id
                WHERE b.user_id = %d",
                $user_id
            )
        );

        $matching_amt = 200;
        $query = $wpdb->prepare(
            "SELECT
                user_id, unilevel, `position`, if( (`position`%2 = 0), 0,1) AS leg, $matching_amt AS amount, level, nodes, node,
                parent_node, code, user_email AS email, user_login AS username,
                user_parent_id AS parent_id,
                user_referrer_id AS referrer_id,
                CONCAT(user_firstname, ' ', user_lastname) AS name,
                user_activated AS activated_at, DATE(user_activated) AS `day`, @pv:=CONCAT(@pv, ',', `position`) AS `binary`
                FROM (
                    SELECT b.*, u.*, c.code FROM eee_binaryv1 AS b
                    LEFT JOIN eee_users AS u ON b.user_id = u.ID
                    LEFT JOIN eee_codes AS c ON b.user_id = c.user_id
                    WHERE unilevel = %d AND `position` >= %d
                    ORDER BY parent_node, level, unilevel, user_activated
                ) AS binary_sorted,
                (SELECT @pv := %d) AS binary_init
                WHERE find_in_set(parent_node, @pv)",
            $top->unilevel, $top->position, $top->position
        );
        //sd( $query );
        $lines = $wpdb->get_results($query);

        return $this->process_leadership_matching( $lines );
    }

    private function process_leadership_matching( $lines ){
        $left = [];
        $left_nodes = [];
        $right = [];
        $right_nodes = [];
        $first_level = $lines[0]->level;
        //sd( $highest );
        foreach($lines as $line) {
            if( $line->level == $first_level ){
                if( $line->leg > 0 ) {
                    $right[] = $line;
                    $right_nodes[] = $line->position;
                }else {
                    $left[] = $line;
                    $left_nodes[] = $line->position;
                }
            }

            if ( in_array( $line->parent_node, $right_nodes ) ) {
                $right[] = $line;
                $right_nodes[] = $line->position;
            }elseif ( in_array( $line->parent_node, $left_nodes ) ) {
                $left[] = $line;
                $left_nodes[] = $line->position;
            }
        }

        if (count($right) > count($left)) {
            $highest =& $right;
            $lowest =& $left;
        } else {
            $highest =& $left;
            $lowest =& $right;
        }

        if ($highest[0]->leg > 0) {
            $pos1 = 'right';
            $pos2 = 'left';
        } else {
            $pos1 = 'left';
            $pos2 = 'right';
        }

        foreach ($highest as $high) {
            foreach ($lowest as $key => $low) {
                if ($high->activated_at === $low->activated_at) {
                    $high->{$pos1} = $high->amount;
                    $high->{$pos2} = $low->amount;
                    unset($lowest[$key]);
                    $pairings[] = $high;
                    break;
                } else {
                    if (!isset($lowest[$key]->{$pos1})) {
                        $lowest[$key]->{$pos1} = 0;
                    }
                    if (!isset($lowest[$key]->{$pos2})) {
                        $lowest[$key]->{$pos2} = $low->amount;
                    }
                }
            }

            if (!isset($high->{$pos1})) {
                $high->{$pos1} = $high->amount;
                $high->{$pos2} = 0;
                $pairings[] = $high;
            }
        }

        $items = array_merge($highest, $lowest);
        usort($items, function ($a, $b) {
            return $a->activated_at > $b->activated_at;
        });
        //sd($items);
        if (count($items) > 0) {

            $matchings = [];
            foreach ( $items as $key => $item ) {
                //echo '<pre>'.print_r( $item, true ).'</pre>';
                $day               = (string) $item->day;
                $amount            = (float) $this->process_pairing( $less, $item);

                $matchings[ $day ] = (object) [
                    'total' => ( isset( $matchings[ $day ]->total ) ? $matchings[ $day ]->total : 0 ) + $amount,
                    'day'   => $day
                ];
            }

            return $matchings;
        }
    }

    private function process_pairing (&$less, &$item)
    {
        $left = 0;
        $right = 0;

        if (($less['right'] + $item->right) >= ($less['left'] + $item->left)) {
            $left = $less['left'] + $item->left;
        } else {
            $right = $less['right'] + $item->right;
        }

        $less['right'] = ($less['right'] + $item->right) - ($left ?: $right);
        $less['left'] =	($less['left'] + $item->left) - ($left ?: $right);

        return ($left+$right);// > $maxPairing ? $maxPairing : $left+$right;
    }

    public function topmax () {
        if (!$this->is_active) return;

        $upline = $this->get_topmax( $this->user->ID );
        if( !isset( $upline['user_id'] ) ) return;

        $matchings = $this->get_leadership_matching( $upline['user_id'] );
        if(!$matchings) return;

        foreach($matchings as $matching) {
            if ( $matching->total == 0 ) {
                continue;
            }

            $matching->total = $matching->total * '0.10';

            if ($matching->total > 3000) {
                $flushout = ($matching->total - 3000);
                $matching->total = $matching->total - $flushout;
            }

            $this->rewards['topmax']['rewards'][$matching->day] = absint($matching->total);
            $this->rewards['topmax']['total'] += $matching->total;
            $this->rewards['total'] += $matching->total;

            if (isset($flushout)) {
                $this->rewards['topmax']['flushouts'][$matching->day] = $flushout;
                $this->rewards['topmax']['flushout'] += $flushout;
                //$this->rewards['flushout'] += $flushout;
            }

            $this->available['topmax'] += $matching->total;
            $this->available['total'] += $matching->total;
        }
    }

    public function get_topmax ($user_id) {
        global $wpdb;

        return $wpdb->get_row(
            $wpdb->prepare(
                "SELECT t.user_id, t.unilevel, t.`position` FROM eee_binaryv1 AS t
                INNER JOIN eee_binaryv1 AS c ON t.`position`=c.parent_node
                AND t.unilevel=c.unilevel
                WHERE c.user_id = %d",
                $user_id
            ), ARRAY_A
        );

        /*$matching_amt = 150;
        $lines = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT
                user_id, unilevel, position, if( (position%2 = 0), 0,1) AS leg, $matching_amt AS amount, level, nodes, node,
                parent_node, code, user_email AS email, user_login AS username,
                user_parent_id AS parent_id,
                user_referrer_id AS referrer_id,
                CONCAT(user_firstname, ' ', user_lastname) AS name,
                user_activated AS activated_at, DATE(user_activated) AS `day`, @pv:=CONCAT(@pv, ',', `position`) AS `binary`
                FROM (
                    SELECT b.*, u.*, c.code FROM eee_binaryv1 AS b
                    LEFT JOIN eee_users AS u ON b.user_id = u.ID
                    LEFT JOIN eee_codes AS c ON b.user_id = c.user_id
                    WHERE unilevel = %d AND POSITION >= %d
                    ORDER BY parent_node, level, unilevel, user_activated
                ) AS binary_sorted,
                (SELECT @pv := %d) AS binary_init
                WHERE find_in_set(parent_node, @pv)",
                $top->unilevel, $top->position, $top->position
            )
        );

        return $this->process_leadership_matching( $lines );*/
    }

    public static function get_rewards ($request) {
        $authentication =& Authentication::get_instance();
        if (method_exists($request, 'get_param')) {
            $user_id = $request->get_param('user_id') ?: $authentication->get_id();
        } elseif (is_numeric($request)) {
            $user_id = $request;
        }
        if (!$user_id) return;

        $rewards = new self($user_id);

        return $rewards->rewards;
    }

    public static function get_available_rewards ($request) {
        $authentication =& Authentication::get_instance();
        $user_id = $request->get_param('user_id') ?: $authentication->get_id();
        if (!$user_id) return;

        $rewards = new self($user_id);

        return $rewards->available;
    }

    public function createhistory ()
    {
        global $wpdb;

        $users = $wpdb->get_col(
            "SELECT ID FROM eee_users AS u
            LEFT JOIN eee_binaryv1 AS b ON b.user_id = ID
            LEFT JOIN eee_codes AS c ON c.user_id = ID"
        );

        foreach($users as $user) {
            $rewards = $this->get_rewards($user);

            if ($rewards['total'] > 0) {
                $this->insert_to_old_rewards($user, $rewards);
            }
        }
        echo 'All Done';
    }

    private function insert_to_old_rewards ($user_id, $rewards)
    {
        global $wpdb;

        $rewards = json_encode($rewards);
        $wpdb->insert(
            'eee_old_rewards',
            array('user_id' => $user_id, 'added_at' => current_time('mysql'), 'rewards' => $rewards),
            array('%d', '%s', '%s')
        );
    }

    public static function &get_instance () {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
