<?php
namespace Stragidex\Models;
use Stragidex\Emelem\Authentication;
use Stragidex\Emelem\Codes;
use \WP_REST_Response;
use \WP_Error;

class Treesv1 {
    static $_instance;
    public $codes;
    public $pairing = [
        'left' => 0,
        'right' => 0
    ];

    public function __construct ()
    {
        self::$_instance =& $this;
        $this->codes =& Codes::get_instance();
    }

    public static function create_unilevel ($user) {
        global $wpdb;

        $wpdb->insert('eee_binaryv1',
            [
                'user_id' => $user->ID,
                'unilevel' => $user->ID,
                'position' => 1,
                'level' => 0,
                'nodes' => 1,
                'node' => 0,
                'parent_node' => 0
            ],
            ['%d', '%d', '%d', '%d', '%d', '%d', '%d']
        );
    }

    public static function create_branch ($args) {
        global $wpdb;

        $wpdb->insert('eee_binaryv1',
            [
                'user_id' => $args->user_id,
                'unilevel' => $args->unilevel,
                'position' => $args->position,
                'level' => $args->level,
                'nodes' => $args->nodes,
                'node' => $args->node,
                'parent_node' => $args->parent_node
            ],
            ['%d', '%d', '%d', '%d', '%d', '%d', '%d']
        );
    }

    public function get_binary_tree ($request)
    {
        global $wpdb;
        $authentication =& Authentication::get_instance();
        $current_logged_id = $authentication->get_id();
        $user_id = $request->get_param('user_id');
        $no_top = true;
        if (!$user_id) {
            $no_top = false;
            $user_id = $current_logged_id;
        }

        //sd( $no_top );
        $top = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT
                u.ID AS user_id, unilevel, position, level, nodes, node, parent_node, code,
                u.user_parent_id AS parent_id, u.user_referrer_id AS referrer_id,
                u.user_login AS username, u.user_email AS email, u.user_membership AS membership,
                CONCAT(u.user_firstname, ' ', u.user_lastname) AS name,
                u.user_activated AS activated_at
                FROM eee_binaryv1 AS b
                LEFT JOIN eee_users AS u ON b.user_id = u.ID
                LEFT JOIN eee_codes AS c ON b.user_id = c.user_id
                WHERE b.user_id = %d",
                $user_id
            )
        );

        if (!$top) return;

        $top_level = 0;
        $max_top_level = 5;
        $top_level_user_id = 0;

        if( $no_top ){
            if ( $top->level > 5 )
                $top_level = abs($top->level - $max_top_level);

            $query = $wpdb->prepare(
                "SELECT
                user_id, unilevel, position, if( (position%2 = 0), 0,1) AS leg, level, nodes, node,
                parent_node, code, user_email AS email, user_login AS username,
                user_parent_id AS parent_id,
                user_referrer_id AS referrer_id,
                CONCAT(user_firstname, ' ', user_lastname) AS name,
                user_activated AS activated_at, @pv:=CONCAT(@pv, ',', `parent_node`) AS `binary`,
                @inset:=find_in_set(position, @pv) AS in_set
                FROM (
                    SELECT b.*, u.*, c.code FROM eee_binaryv1 AS b
                    LEFT JOIN eee_users AS u ON b.user_id = u.ID
                    LEFT JOIN eee_codes AS c ON b.user_id = c.user_id
                    WHERE unilevel = %d AND position <= %d
                    ORDER BY parent_node, level, unilevel
                ) AS binary_sorted,
                (SELECT @pv := %d, @inset:= 1) AS binary_init
                WHERE @inset AND LEVEL = %d",
                $top->unilevel, $top->parent_node, $top->parent_node, $top_level
            );
            //return $query;
            $top_lines = $wpdb->get_results(
                $query, ARRAY_A
            );

            $top_level_count = sizeof( $top_lines );
            $top_level_user_id = isset($top_lines[ $top_level_count - 1 ]['user_id']) ?
                $top_lines[ $top_level_count - 1 ]['user_id'] : 0;
        }

        //if( $top_level_user_id == $current_logged_id ) $top_level_user_id = 0;

        //$level_limit = 15;
        $lines = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT
                user_id, unilevel, position, if( (position%2 = 0), 0,1) AS leg, level, nodes, node,
                parent_node, code, user_email AS email, user_login AS username,
                user_parent_id AS parent_id,
                user_referrer_id AS referrer_id,
                CONCAT(user_firstname, ' ', user_lastname) AS name,
                user_activated AS activated_at, @pv:=CONCAT(@pv, ',', `position`) AS `binary`
                FROM (
                    SELECT b.*, u.*, c.code FROM eee_binaryv1 AS b
                    LEFT JOIN eee_users AS u ON b.user_id = u.ID
                    LEFT JOIN eee_codes AS c ON b.user_id = c.user_id
                    WHERE unilevel = %d AND position >= %d
                    ORDER BY parent_node, level, unilevel
                ) AS binary_sorted,
                (SELECT @pv := %d) AS binary_init
                WHERE find_in_set(parent_node, @pv)",
                $top->unilevel, $top->position, $top->position
            )
        );
        //sd( $lines );
        array_unshift($lines, $top);
        $btree = $this->process_binary_tree($lines);

        return new WP_REST_Response(['items'=> $btree, 'pairing'=>$this->pairing, 'top'=>$top_level_user_id], 200);
    }

    private function process_binary_tree ($lines) {
        //$authentication =& Authentication::get_instance();
        //$user_id = $authentication->get_id();

        $level_limit = 5;
        $btree = array($lines[0]->level => [$lines[0]->position => $lines[0]]);
        $unilevel = absint($lines[0]->unilevel);
        $lowest = end($lines);
        $start = $lines[0]->position;
        $end = ($lines[count($lines)-1]->level - $lines[0]->level) + 2;

        $i = $lines[0]->level;
        $left = [];
        $right = [];
        $level_end = $lines[0]->level + $level_limit;
        $first_level = $lines[0]->level + 1;
        //sd($level_end);
        foreach($lines as $c => $line) {
            if( $line->level == $first_level ){
                if( $line->leg > 0 )
                    $right[] = $line->position;
                else
                    $left[] = $line->position;
            }

            if ( in_array( $line->parent_node, $right ) )
                $right[] = $line->position;
            elseif ( in_array( $line->parent_node, $left ) )
                $left[] = $line->position;

            if( $line->level > $level_end ) continue;

            $btree[$line->level][$line->position] = $line;

            $child_left = $line->position * 2;
            $child_right = $child_left + 1;
            $child_left_node = $line->node * 2;
            $chil_right_node  = $child_left_node + 1;

            $btree[$line->level+1][$child_left] = (object) [
                'available' => true,
                'unilevel' => $unilevel,
                'position' => $child_left,
                'level' => absint($line->level) + 1,
                'nodes' => absint($line->nodes) * 2,
                'node' => $child_left_node,
                'parent_node' => absint($line->position)
            ];

            $btree[$line->level+1][$child_right] = (object) [
                'available' => true,
                'unilevel' => $unilevel,
                'position' => $child_right,
                'level' => absint($line->level) + 1,
                'nodes' => absint($line->nodes) * 2,
                'node' => $chil_right_node,
                'parent_node' => absint($line->position)
            ];
        }

        $this->pairing['left'] = count( $left );
        $this->pairing['right'] = count( $right );

        $btree = array_values($btree);

        $columns = 2;
        foreach($btree as $key => $branch) {
            if ($key === 0) continue;

            $top = $btree[$key-1][key($btree[$key-1])];

            $line = $branch[key($branch)];
            $i = $top->position * 2;
            while (count($btree[$key]) < $columns) {
                if (!isset($btree[$key][$i])) {

                    $btree[$key][$i] = (object) [
                        'unilevel' => $unilevel,
                        'position' => absint($i),
                        'level' => absint($line->level),
                        'nodes' => absint($line->nodes),
                        'node' => $i - $line->nodes,
                        'parent_node' => ($i%2 === 0) ? $i/2 : ($i-1) / 2
                    ];
                }
                $i++;
            }
            ksort( $btree[$key] );
            $columns *= 2;
        }

        if ($end < 7) {
            $btree = array_slice($btree, 0, $end);
        } else {
            $btree = array_slice($btree, 0, 6);
        }

        return $btree;
    }

    private function create_dummy_users ()
    {
        global $wpdb;
        $referrer = 1;
        $parent = 0;
        for($i=1;$i<=20;$i++) {
            if ($i<3 && $i > 0) {
                $parent = 1;
            }
            if ($i===5) {
                $parent = 3;
            }

            $wpdb->insert('eee_users', [
                'user_parent_id' => $parent,
                'user_referrer_id' => $referrer,
                'user_login' => 'derik' . $i,
                'user_pass' => '$P$B1ZtEwRKyGyxcwdl.ssbMTNA1bRPtf.',
                'user_email' => 'test' . $i . '@test.com',
                'user_firstname' => 'Derik' . $i,
                'user_lastname' => 'Seno' . $i,
                'user_phone' => '09176340194',
                'user_registered' => current_time('mysql'),
                'user_membership' => 0,
                'user_role' => 0,
                'user_status' => 1
            ]);

            $parent = 0;
            $referrer = rand(0, $i);
        }
    }

    public static function &get_instance () {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
