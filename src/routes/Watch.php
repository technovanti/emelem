<?php
namespace Stragidex\Routes;
use Stragidex\Models\Watch as Model;
use \WP_REST_Controller;
use \WP_REST_Response;
use \WP_Error;

class Watch extends WP_REST_Controller {
	protected $users;
	private $codes;

	public function __construct ()
	{
		$this->namespace = 'emelem/v2';
		$this->rest_base = 'watch';

        $this->watch =& Model::get_instance();
	}

	public function register_routes ()
	{
        register_rest_route($this->namespace, '/' . $this->rest_base, array(
			array(
				'methods' => 'POST',
				'callback' => array($this->watch, 'add_watch')
			),
			array(
				'methods' => 'GET',
				'callback' => array($this->watch, 'get_watch')
			)
		));
	}
}
