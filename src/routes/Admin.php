<?php
namespace Stragidex\Routes;
use Stragidex\Models\Admin as Model;
use \WP_REST_Controller;
use \WP_REST_Response;
use \WP_Error;

class Admin extends WP_REST_Controller {
  protected $encashments;

  public function __construct ()
  {
    $this->namespace = 'emelem/v2';
    $this->rest_base = 'admin';
    $this->model = Model::get_instance();
  }

  public function register_routes ()
  {
    register_rest_route($this->namespace, '/' . $this->rest_base, array(
      array(
        'methods' => 'GET',
        'callback' => array($this->model, 'any_method')
      )
    ));

    register_rest_route($this->namespace, '/' . $this->rest_base . '/encashments', array(
      array(
        'methods' => 'GET',
        'callback' => array($this->model, 'get_encashments')
      ),
      array(
        'methods' => 'PUT',
        'callback' => array($this->model, 'edit_encashments')
      ),
      array(
        'methods' => 'DELETE',
        'callback' => array($this->model, 'delete_encashments')
      )
    ));

    register_rest_route($this->namespace, '/' . $this->rest_base . '/encashmentDates', array(
      array(
        'methods' => 'GET',
        'callback' => array($this->model, 'get_encashment_dates')
      )
    ));

    register_rest_route($this->namespace, '/' . $this->rest_base . '/rewards/(?P<user_id>\w+)', array(
      array(
        'methods' => 'GET',
        'callback' => array($this->model, 'get_rewards_report')
      )
    ));
  }
}
