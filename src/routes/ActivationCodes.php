<?php
namespace Stragidex\Routes;
use Stragidex\Emelem\Authentication;
use Stragidex\Emelem\Codes;
use Stragidex\Models\Users;
use \WP_REST_Controller;
use \WP_REST_Response;
use \WP_Error;

class ActivationCodes extends WP_REST_Controller {
    protected $users;
    private $codes;

    public function __construct ()
    {
        $this->namespace = 'emelem/v2';
        $this->rest_base = 'activationcodes';

        $this->codes =& Codes::get_instance();
        $this->users =& Users::get_instance();
    }

    public function register_routes ()
    {
        register_rest_route($this->namespace, '/' . $this->rest_base, array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'getCodes')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/getCodesHistory', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'getCodesHistory')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/admin', array(
            array(
                'methods' => 'POST',
                'callback' => array($this, 'admin_generate_codes')
            ),
            array(
                'methods' => 'GET',
                'callback' => array($this, 'admin_get_codes')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/getAvailableCodes', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'getAvailableCodes')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/transferCode', array(
            array(
                'methods' => 'POST',
                'callback' => array($this->users, 'transfer_code')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/activateUser', array(
            array(
                'methods' => 'POST',
                'callback' => array($this->users, 'activate_user')
            )
        ));
    }

    public function admin_get_codes ()
    {
        global $wpdb;

        $authentication =& Authentication::get_instance();
        if ($authentication->get_role() < 1) return new WP_Error(
            'lowlevel',
            __('This is secured!'),
            array('status' => 403)
        );

        $codes = $wpdb->get_results(
            "SELECT code_id, code, IFNULL(s.user_login, 'System') AS source, o.user_login AS owner, u.user_login AS used,
            c.code_created AS created_at
            FROM eee_codes AS c LEFT JOIN eee_users AS s ON s.ID = c.source_user_id
            LEFT JOIN eee_users AS o ON o.ID = c.owner_user_id LEFT JOIN eee_users AS u ON u.ID = c.user_id ORDER BY c.code_created DESC"
        );

        return new WP_REST_Response($codes, 200);
    }

    public function admin_generate_codes ($request)
    {
        $codes = array();

        $authentication =& Authentication::get_instance();
        if ($authentication->get_role() < 1) return new WP_Error(
            'lowlevel',
            __('This is secured!'),
            array('status' => 403)
        );

        $quantity = absint($request->get_param('quantity'));
        $prefix = $request->get_param('prefix');
        $owner = null;

        $owner_login = $request->get_param('username');
        if ($owner_login) {
            $users =& Users::get_instance();
            $owner = $users->get_user_by_login($owner_login);
            if ($owner == null) {
                return new WP_Error(
                    'username_inavalid',
                    __('Invalid username', 'emelem'),
                    array(
                        'status' => 403
                    )
                );
            }

            $owner = $owner->ID;
        }

        for($i=0; $i<$quantity; $i++) {
            $codes[] = $this->codes->create($prefix, $owner);
        }

        return new WP_REST_Response($codes, 200);
    }
}
