<?php
namespace Stragidex\Routes;
use Stragidex\Emelem\Authentication;
use Stragidex\Models\Rewards as Model;
use Stragidex\Models\Rewardsv1;
use \WP_REST_Controller;
use \WP_REST_Response;
use \WP_Error;

class Rewards extends WP_REST_Controller {
    protected $rewards;

    public function __construct ()
    {
        $this->namespace = 'emelem/v2';
        $this->rest_base = 'rewards';
        $this->rewards = Model::get_instance();
        // $this->rewardsv1 = Rewardsv1::get_instance();
    }

    public function register_routes ()
    {
        register_rest_route($this->namespace, '/' . $this->rest_base, array(
            array(
                'methods' => 'GET',
                'callback' => array($this->rewards, 'get_rewards')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/(?P<user_id>\d+)', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->rewards, 'get_rewards')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/getAvailableRewards', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->rewards, 'get_available_rewards')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/watchvideo', array(
            array(
                'methods'             => 'GET',
                'callback'            => array($this->rewards, 'get_watchvideo')
            ),
            array(
                'methods'             => 'POST',
                'callback'            => array($this->rewards, 'add_watchvideo')
            )
        ));
    }
}
