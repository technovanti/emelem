<?php
namespace Stragidex\Routes;
use Stragidex\Emelem\Authentication;
use Stragidex\Models\Encashments as Model;
use \WP_REST_Controller;
use \WP_REST_Response;
use \WP_Error;

class Encashments extends WP_REST_Controller {
	protected $encashments;

	public function __construct ()
	{
		$this->namespace = 'emelem/v2';
		$this->rest_base = 'encashments';
		$this->encashments =& Model::get_instance();
	}

	public function register_routes ()
	{
		register_rest_route($this->namespace, '/' . $this->rest_base, array(
            array(
				'methods' => 'POST',
				'callback' => array($this->encashments, 'add_encashment')
			),
			array(
				'methods' => 'GET',
				'callback' => array($this->encashments, 'get_encashments')
			)
		));
	}
}
