<?php
namespace Stragidex\Routes;
use Stragidex\Emelem\Authentication;
use Stragidex\Models\Trees as Model;
use Stragidex\Models\Treesv1;
use \WP_REST_Controller;
use \WP_REST_Response;
use \WP_Error;

class Trees extends WP_REST_Controller {
    protected $users;
    protected $trees;

    public function __construct ()
    {
        $this->namespace = 'emelem/v2';
        $this->rest_base = 'trees';
        $this->trees = Model::get_instance();
    }

    public function register_routes ()
    {
        register_rest_route($this->namespace, '/' . $this->rest_base, array(
            array(
                'methods' => 'GET',
                'callback' => array($this->trees, 'get_downlines')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/(?P<user_id>\d+)', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->trees, 'get_downlines')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/legs', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->trees, 'get_legs_count')
            )
        ));

        // register_rest_route($this->namespace, '/' . $this->rest_base . '/debug/(?P<user_id>\d+)', array(
        //     array(
        //         'methods' => 'GET',
        //         'callback' => array($this->trees, 'get_downlines')
        //     )
        // ));
    }
}
