<?php
namespace Stragidex\Routes;
use \WP_REST_Controller;
use \WP_REST_Response;
use \WP_Error;

class Users extends WP_REST_Controller {
    protected $users;

    public function __construct ()
    {
        $this->namespace = 'emelem/v2';
        $this->rest_base = 'users';
        $this->users =& \Stragidex\Models\Users::get_instance();
    }

    public function register_routes ()
    {
        register_rest_route($this->namespace, '/' . $this->rest_base, array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'get')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/getSubUsers', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'get_sub_users')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/add', array(
            array(
                'methods' => 'POST',
                'callback' => array($this->users, 'add')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/admin', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'get_admin')
            ),
            array(
                'methods' => 'POST',
                'callback' => array($this->users, 'add_admin')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/getInactiveUsers', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'get_verified_inactive_users')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/getActiveUsers', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'get_active_users')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/getReferrer', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'getReferrer')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/setReferrer', array(
            array(
                'methods' => 'POST',
                'callback' => array($this->users, 'setReferrer')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/search', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'search')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/search/getAll', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'get_users_for_valid_user')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/watched', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'get_watched_today')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/getDailyVideo', array(
            array(
                'methods' => 'GET',
                'callback' => array($this->users, 'get_dailyvideo')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/update_password', array(
            array(
                'methods' => 'POST',
                'callback' => array($this->users, 'update_password')
            )
        ));
    }
}
