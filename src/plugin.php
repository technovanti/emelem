<?php

require_once __DIR__ . '/includes/vendor/autoload.php';

$plugin = new Stragidex\Emelem\Plugin(
    dirname(__DIR__)
);

return $plugin;
