<?php

if (!function_exists('sd')) :
    function sd ($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        exit;
    }
endif;

if (!function_exists('is_emelem')) :
    function is_emelem() {
        global $wp_query;

        if ( ! (isset($wp_query->is_emelem) && $wp_query->is_emelem) ) return false;

        return true;
    }
endif;

if (!function_exists('emelem_get_user_ip')) :
    function emelem_get_user_ip () {
        $ip = $_SERVER['REMOTE_ADDR'];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
        if (!empty( $_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

        return $ip;
    }
endif;
