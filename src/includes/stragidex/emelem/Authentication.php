<?php
namespace Stragidex\Emelem;
use Stragidex\Models\Users;
use \Firebase\JWT\JWT;
use \WP_REST_Controller;
use \WP_REST_Response;
use \WP_Error;

/**
 * JWT Auth for WP
 */
class Authentication {
    static $_instance;
    private $error = null;
    private $id;
    private $activated_at;
    private $role;
    private $parent_id;

    public function __construct ()
    {
        self::$_instance =& $this;
        $this->namespace = 'emelem/v2';
        $this->rest_base = 'auth';

        add_filter('rest_api_init', array(&$this, 'add_cors_support'));
        add_filter('rest_pre_dispatch', array(&$this, 'rest_pre_dispatch'), 10, 3);
        add_filter('determine_current_user', array(&$this, 'determine_current_user'), 10);
    }

    public function register_routes ()
    {
        register_rest_route($this->namespace, '/' . $this->rest_base . '/login', array(
            array(
                'methods' => 'POST',
                'callback' => array($this, 'generate_token')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/validate', array(
            array(
                'methods' => 'POST',
                'callback' => array($this, 'validate_token')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/user', array(
            array(
                'methods' => 'GET',
                'callback' => array($this, 'get_user')
            )
        ));

        register_rest_route($this->namespace, '/' . $this->rest_base . '/resetPassword', array(
            array(
                'methods' => 'POST',
                'callback' => array($this, 'reset_password')
            )
        ));
    }

    public function add_cors_support ()
    {
        $enable_cors = defined('JWT_AUTH_CORS_ENABLE') ? JWT_AUTH_CORS_ENABLE : false;
        if ($enable_cors) {
            $headers = apply_filters('emelem/jwt_auth_cors_allow_headers', 'Access-Control-Allow-Headers, Content-Type, Authorization');
            header(sprintf('Access-Control-Allow-Headers: %s', $headers));
        }
    }

    public function determine_current_user ($user)
    {
        $rest_api_slug = rest_get_url_prefix();
        $valid_api_uri = strpos($_SERVER['REQUEST_URI'], $rest_api_slug);

        if (!$valid_api_uri) {
            return $user;
        }

        $validate_uri = strpos($_SERVER['REQUEST_URI'], 'auth/validate');
        if ($validate_uri > 0) {
            return $user;
        }

        $token = $this->validate_token(false);
        if (is_wp_error($token)) {
            if ($token->get_error_code() !== 'no_auth_header') {
                $this->error = $token;
                return $user;
            } else {
                return $user;
            }
        }
        return $token->data->user->id;
    }

    public function generate_token ($request)
    {
        global $wpdb;

        $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;

        $username = sanitize_user($request->get_param('username'));
        $password = trim($request->get_param('password'));

        if (!$secret_key) {
            return new WP_Error(
                'auth_bad_config',
                __('JWT_AUTH_SECRET_KEY is not set, please contact the admin.', 'emelem'),
                array(
                    'status' => 403,
                )
            );
        }

        $users =& Users::get_instance();
        $user = $users->authenticate($username, $password);

        if (is_wp_error($user)) {
            $error_code = $user->get_error_code();
            return new WP_Error(
                $error_code,
                $user->get_error_message($error_code),
                array(
                    'status' => 403,
                )
            );
        }

        $issuedAt = time();
        $expire = $issuedAt + (DAY_IN_SECONDS * 7);

        $token = array(
            'iss' => get_bloginfo('url'),
            'iat' => $issuedAt,
            'nbf' => $issuedAt,
            'exp' => $expire,
            'data' => array(
                'user' => array(
                    'id' => $user->ID,
                    'role' => $user->user_role,
                    'parent_id' => $user->user_parent_id,
                    'activated_at' => $user->user_activated
                )
            )
        );

        $token = JWT::encode($token, $secret_key);

        $data = array(
            'token' => $token,
            'role' => $user->user_role,
            'id' => $user->ID,
            'activated_at' => $user->user_activated
        );

        if (!isset($user->type)) {
            $ip = emelem_get_user_ip();
            $last_login = current_time('mysql');

            $wpdb->update('eee_users', array( 'ip' => $ip, 'last_login' => $last_login), array( 'ID' => $user->ID ));
        }

        return new WP_REST_Response($data, 200);
    }

    public function validate_token ($output = true)
    {
        $auth = isset($_SERVER['HTTP_AUTHORIZATION']) ? $_SERVER['HTTP_AUTHORIZATION'] : false;

        if (!$auth) {
            $auth = isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) ? $_SERVER['REDIRECT_HTTP_AUTHORIZATION'] : false;
        }

        if (!$auth) {
            return new WP_Error(
                'no_auth_header',
                'Authorization header not found.',
                array(
                    'status' => 403,
                )
            );
        }

        list($token) = sscanf($auth, 'Bearer %s');
        if (!$token) {
            return new WP_Error(
                'auth_header_invalid',
                'Authorization header malformed.',
                array(
                    'status' => 403,
                )
            );
        }

        $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
        if (!$secret_key) {
            return new WP_Error(
                'auth_bad_config',
                __('JWT_AUTH_SECRET_KEY is not set, please contact the admin.', 'emelem'),
                array(
                    'status' => 403,
                )
            );
        }

        try {
            $token = JWT::decode($token, $secret_key, array('HS256'));
            if ($token->iss !== get_bloginfo('url')) {
                return new WP_Error(
                    'auth_bad_iss',
                    'The iss do not match with this server',
                    array(
                        'status' => 403,
                    )
                );
            }

            if (!isset($token->data->user->id)) {
                return new WP_Error(
                    'auth_bad_request',
                    'User ID not found in the token',
                    array(
                        'status' => 403,
                    )
                );
            }

            $this->id = $token->data->user->id;

            if (!isset($token->data->user->role)) {
                return new WP_Error(
                    'auth_bad_request',
                    'Role not found in the token.',
                    array(
                        'status' => 403,
                    )
                );
            }
            $this->role = $token->data->user->role;

            $cookie = $_COOKIE['EMELEM_SECURE_AUTH_COOKIE'];
            $cookie_elements = json_decode(stripslashes_deep($cookie));
            if ($token->data->user->role !== $cookie_elements->role) {
                return new WP_Error(
                    'auth_hack',
                    'The user and role did not match with this server. Logging trying to accesss malformed auth.',
                    array(
                        'status' => 403,
                    )
                );
            }

            if ($token->data->user->role > 2) {
                $wp_user = get_userdata($token->data->user->id);
                if (is_wp_error($wp_user)) {
                    return new WP_Error(
                        'auth_bad_request',
                        'The user and role did not match with this server.',
                        array(
                            'status' => 403,
                        )
                    );
                }

                if (!in_array('administrator', $wp_user->roles)) {
                    return new WP_Error(
                        'auth_bad_request',
                        'You are trying to cheat. Saving to logs.',
                        array(
                            'status' => 403,
                        )
                    );
                }
            }

            // Set Parent ID
            $this->parent_id = ( !$token->data->user->parent_id ? $token->data->user->id : $token->data->user->parent_id );

            $this->activated_at = $token->data->user->activated_at;

            if (!$output) return $token;

            $data = array(
                'code' => 'auth_valid_token',
                'data' => array(
                    'status' => 200,
                )
            );
            return new WP_REST_Response($data, 200);
        } catch (\Exception $e) {
            return new WP_Error(
                'auth_invalid_token',
                $e->getMessage(),
                array(
                    'status' => 403,
                )
            );
        }
    }

    public function reset_password ($request)
    {
        $users =& Users::get_instance();
        return $users->reset_password($request);
    }

    public function get_user ($request)
    {

        if ($this->role === 3) {
            $wp_user = get_userdata($this->id);

            if ($wp_user && in_array('administrator', $wp_user->roles)) {
                $data = array(
                    'id' => $wp_user->ID,
                    'parent' => 0,
                    'referrer' => 0,
                    'login' => $wp_user->data->user_login,
                    'email' => $wp_user->data->user_email,
                    'firstname' => $wp_user->data->display_name,
                    'lastname' => '',
                    'phone' => '',
                    'registered_at' => $wp_user->data->user_registered,
                    'activated_at' => '0000-00-00 00:00:00',
                    'code' => ''
                );

                return new WP_REST_Response($data, 200);
            }
        }

        $users =& Users::get_instance();
        $user = $users->get_user($this->id);

        if ($user !== null) {
            $data = array(
                'id' => $user->ID,
                'parent' => $user->user_parent_id,
                'referrer' => $user->user_referrer_id,
                'login' => $user->user_login,
                'email' => $user->user_email,
                'firstname' => $user->user_firstname,
                'lastname' => $user->user_lastname,
                'phone' => $user->user_phone,
                'registered_at' => $user->user_registered,
                'activated_at' => $user->user_activated,
                'code' => $user->code,
                'status' => $user->user_status
            );

            return new WP_REST_Response($data, 200);
        }

        return new WP_Error('invalid_user', __('No user matched the request.', 'emelem'), ['status' => 403]);
    }

    /**
     * This is the Middleware where the token is verified.
     */
    public function rest_pre_dispatch ($response, $server, $request)
    {
        $excluded = array(
            'emelem/v2/auth/login',
            'emelem/v2/auth/validate',
            'emelem/v2/auth/resetPassword',
            'emelem/v2/users/add',
            'emelem/v2/users/search'
        );

        $excluded = apply_filters('emelem/exclude_auth_header', $excluded);
        foreach($excluded as $route) {
            if (strpos($request->get_route(), $route) > 0) return $response;
        }

        if (strpos($request->get_route(), 'emelem/v2') === false) return $response;
        if ($request->get_route() === '/emelem/v2') return $response;

        if (is_wp_error($this->error)) return $this->error;

        $token = $this->validate_token();
        if (is_wp_error($token)) return $token;

        return $response;
    }

    public function get_id ()
    {
        return $this->id;
    }

    public function get_role ()
    {
        return $this->role;
    }

    public function get_parent_id ()
    {
        return $this->parent_id;
    }

    public function get_activated_at ()
    {
        return $this->activated_at;
    }

    public static function &get_instance()
    {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
