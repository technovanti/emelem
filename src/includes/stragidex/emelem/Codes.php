<?php
namespace Stragidex\Emelem;
use Stragidex\Emelem\Authentication;

use \WP_Error;

class Codes {
    static $_instance;
    protected $length;

    public function __construct ($length = 9)
    {
        self::$_instance =& $this;
        $this->length = $length;
    }

    public function generate ($prefix) {
        global $wpdb;

        if (!$prefix) return new WP_Error('bad_code_prefix', __('Invalid or no prefix in code generation.', 'emelem'), array('status' => 403));

        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($this->length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($this->length / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        $code = strtoupper($prefix . substr(bin2hex($bytes), 0, $this->length));

        while ($this->verify($code)) {
            $code = $this->generate($prefix);
        }

        return $code;
    }

    public function create ($prefix = 'TVM', $owner_user_id = null)
    {
        global $wpdb;

        $code = $this->generate($prefix);
        $authentication =& Authentication::get_instance();
        $source_user_id = $authentication->get_id();

        if (!$owner_user_id) {
            $owner_user_id = $source_user_id;
        }

        $date = current_time('mysql');

        $insert = $wpdb->query(
            $wpdb->prepare(
                "INSERT INTO eee_codes (source_user_id, owner_user_id, code, last_activity, code_created) VALUES (%d, %d, %s, %s, %s)",
                $source_user_id, $owner_user_id, $code, $date, $date
            )
        );

        if ($insert) {
            $this->created_history($code, $date);

            return $code;
        }
    }

    public function created_history ($code, $date = null)
    {
        return $this->add_history('created', $code, $date);
    }

    public function transferred_history ($code, $date = null)
    {
        return $this->add_history('transferred', $code, $date);
    }

    public function activated_history ($code, $date = null)
    {
        return $this->add_history('activated', $code, $date);
    }

    public function add_history ($action, $code, $history_date = null)
    {
        global $wpdb;

        $code_object = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT * FROM eee_codes WHERE code = %s",
                $code
            )
        );

        if ($code_object === null) return new WP_Error(
            'code_invalid',
            __('Code does not exist.', 'emelem'),
            array(
                'status' => 403
            )
        );

        $authentication =& Authentication::get_instance();
        $user_id = $authentication->get_id();
        if (!$user_id) {
            $user_id = $code_object->owner_user_id;
        }

        if (!$history_date) {
            $history_date = current_time('mysql');
        }

        $insert = $wpdb->query(
            $wpdb->prepare(
                "INSERT INTO eee_codehistory (code_id, code_action, user_id, history_date) VALUES (%d, %s, %d, %s)",
                $code_object->code_id,
                $action,
                $user_id,
                $history_date
            )
        );

        if ($insert !== null) return $history_date;
    }

    public function activate ($code, $user_id)
    {
        global $wpdb;

        $check = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT code_id FROM eee_codes WHERE code = %s",
                $code
            )
        );

        if ($check === null) return new WP_Error(
            'code_invalid',
            __('Code does not exist', 'emelem'),
            array(
                'status' => 403
            )
        );

        $check = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT code_id FROM eee_codes WHERE code = %s AND user_id = %d",
                $code, 0
            )
        );

        if ($check === null) return new WP_Error(
            'code_used',
            __('Code already used', 'emelem'),
            array(
                'status' => 403
            )
        );

        $update = $wpdb->update('eee_codes',
            array('user_id' => $user_id),
            array('code' => $code)
        );

        if ($update !== null) return $this->activated_history($code);
    }

    public function verify ($code)
    {
        global $wpdb;

        $check = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT code FROM eee_codes WHERE code = %s",
                $code
            )
        );

        if ($check !== null) return true;

        return false;
    }

    public function user_id ($code)
    {
        global $wpdb;

        if ($this->verify($code)) return false;

        return $wpdb->get_var(
            $wpdb->prepare(
                "SELECT user_id FROM eee_codes WHERE code = %s",
                $code
            )
        );
    }

    public static function &get_instance ()
    {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
