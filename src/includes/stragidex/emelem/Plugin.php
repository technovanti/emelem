<?php

Namespace Stragidex\Emelem;

Use Stragidex\Emelem\Migration;
Use Stragidex\Emelem\Authentication;
use \WP_Error;

/**
 * Emelem Application
 */
class Plugin
{
    static $_instance;
    public $uri;
    public $dashboard = false;

    public function __construct ($dir)
    {
        self::$_instance =& $this;

        $this->uri = get_option('emelem_uri', 'dashboard');
        register_activation_hook( EMELEM_ABSPATH . 'emelem.php', [&$this, 'on_activation'] );
        register_deactivation_hook( EMELEM_ABSPATH . 'emelem.php', [&$this, 'on_deactivation'] );

        add_action('rest_api_init', [&$this, 'register_routes']);

        add_action('init', [&$this, 'referral'], 110);
        add_action('parse_request', [&$this, 'parse_request'], 110, 1);
        add_action('parse_query', [&$this, 'parse_query'], 110, 1);
        add_filter('home_url', [&$this, 'remove_from_admin_redirect']);
        add_filter('site_url', [&$this, 'remove_from_admin_redirect']);
        add_filter('pre_handle_404', [&$this, 'remove_is_404'], 110, 2);

        add_action('template_include', function ($template) {
            if (!is_emelem()) return $template;

            return EMELEM_ABSPATH . 'app/dist/index.html';
        }, 109);
    }

    public function on_activation ()
    {
        update_option('emelem_uri', 'dashboard');
        $migration =& Migration::get_instance();
        $migration->install();

        $this->add_rewrite_rule();
    }

    public function on_deactivation ()
    {
        $migration =& Migration::get_instance();
        $migration->uninstall();

        flush_rewrite_rules(false);
    }

    public function register_routes ()
    {
        $auth = new Authentication;
        $auth->register_routes();

        foreach(glob(EMELEM_ABSPATH . 'src/routes/*.php') as $file) {
            $class = '\\Stragidex\\Routes\\' . basename($file, '.php');
            $controller = new $class;
            $controller->register_routes();
        }
    }

    public function referral () {
        if (isset($_REQUEST['referral_code'])) {
            $users =& \Stragidex\Models\Users::get_instance();
            $sponsor_id = $users->get_user_id_by_code(sanitize_text_field($_REQUEST['referral_code']));

            setcookie('emelem_referrer', $sponsor_id, time() + 2 * DAY_IN_SECONDS);
        }
    }

    public function parse_request ($query)
    {
        $request = explode('/', $query->request);
        if ($request[0] !== $this->uri) return;

        $query->set_query_var('emelem', $this->uri);
    }

    public function parse_query ($query)
    {
        if (!array_key_exists('emelem', $query->query)) return $query;

        $query->is_emelem = true;
    }

    public function remove_from_admin_redirect ($url)
    {
        if (strpos($url, $this->uri) === false) return $url;

        return;
    }

    public function remove_is_404 ($bool, $wp_query)
    {
        if (!is_emelem()) return $bool;

        return true;
    }

    function add_rewrite_rule ()
    {
        add_rewrite_rule('^' . $this->uri . '/(.+?)/(.+?)/?$', 'index.php?emelem=' . $this->uri . '&route=$matches[1]&method=$matches[2]', 'top');
        flush_rewrite_rules(false);
    }

    public function &get_instnace ()
    {
        if (!isset(self::$_instance)) {
            return new WP_Error(
                "Plugin error",
                __("Application has no instance.")
            );
        }

        return self::$_instance;
    }
}
