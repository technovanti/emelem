<?php
Namespace Stragidex\Emelem;

use \WP_Error;

/**
 * Database and table migration
 * @version 0.1.0
 */
class Migration {
    static $_instance;
    public $emelem_db_version = '0.2.0';
    public $prefix = 'eee_';

    public function __construct()
    {
        self::$_instance =& $this;
        update_option('emelem_db_version', '0.2.0');
    }

    public function install ()
    {
        $this->schema();
        update_option('emelem_db_version', $this->emelem_db_version);
        // $this->migrate();
    }

    public function uninstall () {
        //
    }

    public function schema ()
    {
        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();

        $users_table = $this->prefix . 'users';
        $tables = "CREATE TABLE $users_table (
            ID bigint(20) unsigned NOT NULL auto_increment,
            user_parent_id bigint(20) unsigned NOT NULL default '0',
            user_referrer_id bigint(20) unsigned NOT NULL default '0',
            user_login varchar(60) NOT NULL default '',
            user_pass varchar(255) NOT NULL default '',
            user_email varchar(50) NOT NULL default '',
            user_firstname varchar(50) NOT NULL default '',
            user_lastname varchar(50) NOT NULL default '',
            user_phone varchar(50) NOT NULL default '',
            user_activated datetime NOT NULL default '0000-00-00 00:00:00',
            user_registered datetime NOT NULL default '0000-00-00 00:00:00',
            user_activation_key varchar(255) NOT NULL default '',
            user_membership int(11) NOT NULL default '0',
            user_role int(11) NOT NULL default '0',
            user_status int(11) NOT NULL default '0',
            ip varchar(50) NOT NULL default '',
            last_login datetime NOT NULL default '0000-00-00 00:00:00',
            PRIMARY KEY  (ID),
            KEY user_login_key (user_login),
            KEY user_activated (user_activated),
            KEY user_parent_id (user_parent_id),
            KEY user_referrer_id (user_referrer_id),
            KEY user_email (user_email)
        ) $charset_collate;\n";

        $codes_table = $this->prefix . 'codes';
        $tables .= "CREATE TABLE $codes_table (
            code_id bigint(20) unsigned NOT NULL auto_increment,
            source_user_id bigint(20) unsigned NOT NULL default '0',
            user_id bigint(20) unsigned NOT NULL default '0',
            owner_user_id bigint(20) unsigned NOT NULL default '0',
            code varchar(60) NOT NULL default '',
            last_activity datetime NOT NULL default '0000-00-00 00:00:00',
            code_created datetime NOT NULL default '0000-00-00 00:00:00',
            PRIMARY KEY  (code_id),
            UNIQUE KEY code (code),
            KEY user_id (user_id)
        ) $charset_collate;\n";

        $codehistory_table = $this->prefix . 'codehistory';
        $tables .= "CREATE TABLE $codehistory_table (
            chistory_id bigint(20) unsigned NOT NULL auto_increment,
            code_id bigint(20) unsigned NOT NULL default '0',
            code_action varchar(60) NOT NULL default '',
            user_id bigint(20) unsigned NOT NULL default '0',
            history_date datetime NOT NULL default '0000-00-00 00:00:00',
            PRIMARY KEY  (chistory_id),
            KEY code_id (code_id),
            KEY user_id (user_id)
        ) $charset_collate;\n";

        $binary_table = $this->prefix . 'binary';
        $tables .= "CREATE TABLE $binary_table (
            user_id bigint(20) unsigned NOT NULL default '0',
            unilevel int(11) unsigned NOT NULL default '0',
            position bigint(20) unsigned NOT NULL default '0',
            level int(11) unsigned NOT NULL default '0',
            PRIMARY KEY  (unilevel, position),
            KEY unilevel_position_level (unilevel, position, level),
            KEY parent (parent)
        ) $charset_collate;\n";

        $old_rewards_table = $this->prefix . 'old_rewards';
        $tables .= "CREATE TABLE $old_rewards_table (
            ID bigint(20) unsigned NOT NULL auto_increment,
            user_id bigint(20) unsigned NOT NULL default '0',
            added_at datetime NOT NULL default '0000-00-00 00:00:00',
            rewards longtext NOT NULL,
            PRIMARY KEY  (ID),
            UNIQUE KEY user_id (user_id),
            KEY added_at (added_at)
        ) $charset_collate;\n";

        $videos_table = $this->prefix . 'videos';
        $tables .= "CREATE TABLE $videos_table (
            video_id bigint(20) unsigned NOT NULL auto_increment,
            youtube_id varchar(60) NOT NULL default '',
            user_id bigint(20) unsigned NOT NULL default '0',
            added_at datetime NOT NULL default '0000-00-00 00:00:00',
            deleted_at datetime NOT NULL default '0000-00-00 00:00:00',
            PRIMARY KEY  (video_id),
            UNIQUE KEY youtube_id (youtube_id)
        ) $charset_collate;\n";

        $video_watch_table = $this->prefix . 'watches';
        $tables .= "CREATE TABLE $video_watch_table (
            watch_id bigint(20) unsigned NOT NULL auto_increment,
            user_id bigint(20) unsigned NOT NULL default '0',
            youtube_id varchar(60) NOT NULL default '',
            amount decimal(12,2) NOT NULL default '0.00',
            date_added datetime NOT NULL default '0000-00-00 00:00:00',
            PRIMARY KEY  (watch_id),
            KEY video_userid (user_id),
            KEY youtube_id (youtube_id),
            KEY video_date (date_added)
        ) $charset_collate;\n";

        $encashmenthistory_table = $this->prefix . 'encashmenthistory';
        $tables .= "CREATE TABLE $encashmenthistory_table (
            ehistory_id bigint(20) unsigned NOT NULL auto_increment,
            encashment_id bigint(20) unsigned NOT NULL default '0',
            encashment_action varchar(60) NOT NULL default '',
            user_id bigint(20) unsigned NOT NULL default '0',
            history_date datetime NOT NULL default '0000-00-00 00:00:00',
            PRIMARY KEY  (ehistory_id),
            KEY encashment_id_encashment_action (encashment_id, encashment_action),
            KEY encashment_action_user_id (encashment_action, user_id),
            KEY user_id (user_id)
        ) $charset_collate;\n";

        $encashments_table = $this->prefix . 'encashments';
        $tables .= "CREATE TABLE $encashments_table (
            encashment_id bigint(20) unsigned NOT NULL auto_increment,
            user_id bigint(20) unsigned NOT NULL default '0',
            amount decimal(12,2) NOT NULL default '0.00',
            status tinyint(1) NOT NULL default '0',
            created_at datetime NOT NULL default '0000-00-00 00:00:00',
            updated_at datetime NOT NULL default '0000-00-00 00:00:00',
            deleted_at datetime NOT NULL default '0000-00-00 00:00:00',
            PRIMARY KEY  (encashment_id),
            KEY user_id_status (user_id, status)
        ) $charset_collate;\n";

        $encashment_details_table = $this->prefix . 'encashment_details';
        $tables .= "CREATE TABLE $encashment_details_table (
            encashment_id bigint(20) unsigned NOT NULL default '0',
            type varchar(50) NOT NULL default '',
            name varchar(50) NOT NULL default '',
            account varchar(50) NOT NULL default '',
            branch varchar(50) NOT NULL default '',
            PRIMARY KEY  (encashment_id)
        ) $charset_collate;\n";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        ob_start();
        dbDelta($tables);
        ob_end_clean();
    }

    public static function &get_instance ()
    {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
