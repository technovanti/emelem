<?php
namespace Stragidex\Emelem;
use Stragidex\Emelem\Hasher;

class Notifications {
    static $_instance;
    public $hasher;
    public $from;

    public function __construct ()
    {
        self::$_instance =& $this;
        $this->hasher =& Hasher::get_instance();
    }

    // This needs cleaning up next time
    public function new_user ($user)
    {
        global $wpdb;

        $key = $this->hasher->generate_password();
        $hashed = $this->hasher->generate_activation_key_hash($key);

        $wpdb->update('eee_users', array( 'user_activation_key' => $hashed ), array( 'user_login' => $user['user_login'] ) );

        $message  = sprintf( __( 'Username: %s' ), $user['user_login'] ) . "\r\n\r\n";
        $message .= __( 'To set your password, visit the following address:' ) . "\r\n\r\n";
        $message .= "<" . site_url() . "/dashboard/?key=$key&login=" . rawurlencode( $user['user_login'] ) . ">\r\n\r\n";

        $message .= site_url() . "/dashboard/\r\n";

        $this->setFrom(get_bloginfo('name') . ' Team', 'team');

        $wp_new_user_notification_email = array(
            'to'      => $user['user_email'],
            'subject' => sprintf(__( '[%s Platform] Login Details'), get_bloginfo('name')),
            'message' => $message,
            'headers' => $this->from
        );

        wp_mail(
            $wp_new_user_notification_email['to'],
            wp_specialchars_decode( sprintf( $wp_new_user_notification_email['subject'], get_bloginfo('name') ) ),
            $wp_new_user_notification_email['message'],
            $wp_new_user_notification_email['headers']
        );
    }

    public function new_admin ($user)
    {
        global $wpdb;

        $key = $this->hasher->generate_password();
        $hashed = $this->hasher->generate_activation_key_hash($key);

        $wpdb->update('eee_users', array( 'user_activation_key' => $hashed ), array( 'user_login' => $user['user_login'] ) );

        $message  = sprintf( __( 'Username: %s' ), $user['user_login'] ) . "\r\n\r\n";
        $message .= __( 'To set your password, visit the following address:' ) . "\r\n\r\n";
        $message .= "<" . site_url() . "/dashboard/?key=$key&login=" . rawurlencode( $user['user_login'] ) . ">\r\n\r\n";

        $message .= site_url() . "/dashboard/\r\n";

        $this->setFrom(get_bloginfo('name') . ' Team', 'team');

        $wp_new_user_notification_email = array(
            'to'      => $user['user_email'],
            'subject' => sprintf(__( '[%s Platform] Admin Login Details'), get_bloginfo('name')),
            'message' => $message,
            'headers' => $this->from
        );

        wp_mail(
            $wp_new_user_notification_email['to'],
            wp_specialchars_decode( sprintf( $wp_new_user_notification_email['subject'], get_bloginfo('name') ) ),
            $wp_new_user_notification_email['message'],
            $wp_new_user_notification_email['headers']
        );
    }

    public function setFrom ($name = null, $email = null)
    {
        $name = get_bloginfo('name');
        $email = 'wordpress';
        $sitename = strtolower( $_SERVER['SERVER_NAME'] );
        if ( substr( $sitename, 0, 4 ) == 'www.' ) {
            $sitename = substr( $sitename, 4 );
        }

        $name = get_bloginfo('name') . ' Team';
        $email = $email . '@' . $sitename;
        $this->from = 'From: ' . $name . ' <' . $email . '>';
    }

    public static function &get_instance ()
    {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
