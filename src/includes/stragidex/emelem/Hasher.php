<?php
namespace Stragidex\Emelem;

class Hasher {
    static $_instance;

    public function __construct ()
    {
        self::$_instance =& $this;
    }

    public function generate_password ()
    {
        return wp_generate_password(20, false);
    }

    public function generate_activation_key_hash ($key)
    {
        global $wp_hasher;

        if ( empty( $wp_hasher ) ) {
            require_once ABSPATH . WPINC . '/class-phpass.php';
            $wp_hasher = new \PasswordHash( 8, true );
        }
        return time() . ':' . $wp_hasher->HashPassword( $key );
    }

    public function check_password_reset_key ($key, $login)
    {
        global $wpdb, $wp_hasher;

        $key = preg_replace( '/[^a-z0-9]/i', '', $key );

        if ( empty( $key ) || ! is_string( $key ) ) return false;

    	if ( empty( $login ) || ! is_string( $login ) ) return false;

    	$row = $wpdb->get_row( $wpdb->prepare( "SELECT ID, user_pass, user_activation_key FROM eee_users WHERE user_login = %s", $login ) );
    	if ( ! $row ) return false;

    	if ( empty( $wp_hasher ) ) {
    		require_once ABSPATH . WPINC . '/class-phpass.php';
    		$wp_hasher = new \PasswordHash( 8, true );
    	}

        $expiration_duration = DAY_IN_SECONDS;

        if ( false !== strpos( $row->user_activation_key, ':' ) ) {
    		list( $pass_request_time, $pass_key ) = explode( ':', $row->user_activation_key, 2 );
    		$expiration_time                      = $pass_request_time + $expiration_duration;
    	} else {
    		$pass_key        = $row->user_activation_key;
    		$expiration_time = false;
    	}

    	if ( ! $pass_key ) return false;

    	$hash_is_correct = $wp_hasher->CheckPassword( $key, $pass_key );

        if ( $hash_is_correct && $expiration_time && time() < $expiration_time ) {
    		return true;
    	} elseif ( $hash_is_correct && $expiration_time ) {
    		// Key has an expiration time that's passed
    		return false;
    	}

    	return false;
    }

    public static function &get_instance ()
    {
        if (!isset(self::$_instance)) self::$_instance = new self;

        return self::$_instance;
    }
}
