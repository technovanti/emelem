<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit625e92962033a3a2826d4df33e927a3a
{
    public static $files = array (
        '5013ed83e6b3cfe10e15660a8d44e700' => __DIR__ . '/../../../..' . '/src/helpers/functions.php',
    );

    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Stragidex\\Routes\\' => 17,
            'Stragidex\\Models\\' => 17,
            'Stragidex\\Emelem\\' => 17,
        ),
        'F' => 
        array (
            'Firebase\\JWT\\' => 13,
        ),
        'C' => 
        array (
            'Composer\\Installers\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Stragidex\\Routes\\' => 
        array (
            0 => __DIR__ . '/../../../..' . '/src/routes',
        ),
        'Stragidex\\Models\\' => 
        array (
            0 => __DIR__ . '/../../../..' . '/src/models',
        ),
        'Stragidex\\Emelem\\' => 
        array (
            0 => __DIR__ . '/../../../..' . '/src/includes/stragidex/emelem',
        ),
        'Firebase\\JWT\\' => 
        array (
            0 => __DIR__ . '/..' . '/firebase/php-jwt/src',
        ),
        'Composer\\Installers\\' => 
        array (
            0 => __DIR__ . '/..' . '/composer/installers/src/Composer/Installers',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit625e92962033a3a2826d4df33e927a3a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit625e92962033a3a2826d4df33e927a3a::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
