<?php
/**
 * Plugin Name:       Emelem
 * Plugin URI:        https://emelem.stragidex.com/
 * Description:       Binary multi-level marketing platform plugin for WordPress.
 * Version:           0.1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Stragidex
 * Author URI:        https://www.stragidex.com/
 * Text Domain:       emelem
 */

define('EMELEM_ABSPATH', plugin_dir_path(__FILE__));
define('EMELEM_FILE', __FILE__);

define('JWT_AUTH_SECRET_KEY', '+^w;~P@jBm+-&T2A*|yJKjs5<!PpOlqZ+El)ICFme+I|}Q+$.Q0N+aMu<XX_.1BR');
define('JWT_AUTH_CORS_ENABLE', false);

$plugin = require __DIR__ . '/src/plugin.php';

add_filter('emelem/exclude_auth_header', function ($exclude) {
    $exclude[] = 'emelem/v2/users/getReferrer';
    $exclude[] = 'emelem/v2/users/setReferrer';
    $exclude[] = 'emelem/v2/admin/rewards';
    // $exclude[] = 'emelem/v2/admin';
    // $exclude[] = 'emelem/v2/trees/debug';
    // $exclude[] = 'emelem/v2/trees/prev';
    // $exclude[] = 'emelem/v2/trees/reprocess';
    // $exclude[] = 'emelem/v2/rewards/prev';
    // $exclude[] = 'emelem/v2/rewards/createhistory';
    // $exclude[] = 'emelem/v2/rewards/';
    return $exclude;
});
